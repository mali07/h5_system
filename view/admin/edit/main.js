/**
 * Created by chiqingzhen on 04/12/2016.
 */


var Vue = require('vue');
import Main from './componets/main.vue';
import store from './store/index';
import VueSource from 'vue-resource';

import './directives/drag';
import './directives/dragmove';
import './directives/select';
import './directives/upload';
import './directives/keyboard';
import './filters/newline';

Vue.use(VueSource);

Vue.http.options.root = '/admin';
Vue.http.interceptors.push((request, next) => {
    next((response) => {
        if (response.body.errno != 0) {
            response.ok = false;
        } else {
            response.body = response.body.data;
        }
    })
});

new Vue({
    el: '#app',
    render: h => h(Main),
    store
});