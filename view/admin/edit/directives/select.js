/**
 * Created by chiqingzhen on 12/12/2016.
 */


import Vue from 'vue';
import store from '../store/index';



Vue.directive('select', {
    inserted(el, binding) {
        el.onclick = function (e) {
            if (e.ctrlKey || e.metaKey) {
                if (binding.value.multi) {
                    binding.value.multi();
                }
            } else {
                binding.value.single();
            }

            e.stopPropagation();
        }

    }
});