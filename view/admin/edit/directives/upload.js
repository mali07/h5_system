/**
 * Created by chiqingzhen on 13/12/2016.
 */


import Vue from 'vue';
import store from '../store/index';
import PicService from '@chr/pic';


Vue.directive('upload', {
    inserted(el, binding) {

        var finishCallback = binding.value.finish;
        var startCallback = binding.value.start;

        if (!(typeof finishCallback == 'function' && typeof startCallback == 'function')) {
            throw new Error('neither finish nor start callback cannot be undefined')
        }

        var picService = new PicService('/nowater/chrop/');
        
        el.onclick = function (e) {
            var fileDom = document.createElement('input');
            fileDom.setAttribute('type', 'file');
            fileDom.setAttribute('multiple', true);
            fileDom.setAttribute('style', 'visiable:hidden; position:fixed; ');

            fileDom.onchange = function (e) {
                var fileList = e.target.files;
                startCallback();
                if (fileList.length > 0) {

                    var count = 0;

                    function countFinish() {
                        count ++;
                        if (count == fileList.length) {
                            // finish here
                        }
                    }

                    for(var i = 0; i < fileList.length; i ++) {
                        (function (i) {
                            picService.upload(fileList[i], function (err, fileName) {
                                countFinish();
                                if (fileName) {
                                    finishCallback(fileName, i < fileList.length - 1)
                                }
                            },function(loaded,total){
                                console.log(loaded+ "   "+total);
                            })
                        })(i)
                    }
                }
            };

            document.querySelector('body').appendChild(fileDom);
            fileDom.click();
        }
    }
});