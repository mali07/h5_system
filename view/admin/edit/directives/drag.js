/**
 * Created by chiqingzhen on 09/12/2016.
 */

import Vue from 'vue';
import store from '../store/index';

var direction = {
    N: 'n',
    S: 's',
    E: 'e',
    W: 'w',
    NE: 'ne',
    NW: 'nw',
    SE: 'se',
    SW: 'sw'
};

Vue.directive('drag', {
    inserted(el, binding) {

        var sorwidth = el.parentElement.parentElement.style.width;
        var sorHeight = el.parentElement.parentElement.style.height;


        var start = {
            x: 0,
            y: 0,
        };


        // 只有为 5 的倍数的时候再出发，防止密集计算
        var counter = 0;

        el.setAttribute('draggable', true);

        el.ondragstart = e => {
            start.x = e.pageX;
            start.y = e.pageY;
        };

        el.ondrag = e => {

            e.preventDefault();
            e.stopPropagation();

                 if (counter % 5 == 0) {
                    var offsetYOrigin = e.pageY - start.y;
                    var offsetXOrigin = e.pageX - start.x;
                    start.x = e.pageX;
                    start.y = e.pageY;
                    var offsetY = Math.abs(offsetYOrigin);
                    var offsetX = Math.abs(offsetXOrigin);
                    var isXIncreasing = true;
                    var isYIncreasing = true;

                    if (offsetX > 40 || offsetY > 40) return;

                    if(e.shiftKey){
                        //等比缩放，宽高比
                        var aspectxy= Number(parseInt(sorwidth)/parseInt(sorHeight)).toFixed(2);
                        console.log(aspectxy);
                        switch (binding.value.direction) {
                            case direction.NW:
                                if (offsetYOrigin > 0) {
                                    isYIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    top: (isYIncreasing ? '-' : '+') + offsetY + 'px',
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px',
                                    left: (isYIncreasing ? '-' : '+') + offsetY * aspectxy + 'px',
                                    width: (isYIncreasing ? '+' : '-') + offsetY * aspectxy + 'px',
                                });
                                break;
                            case direction.SW:
                                if (offsetYOrigin < 0) {
                                    isYIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px',
                                    left: (isYIncreasing ? '-' : '+') + offsetY * aspectxy + 'px',
                                    width: (isYIncreasing ? '+' : '-') + offsetY * aspectxy + 'px',
                                });
                                break;
                            case direction.NE:
                                if (offsetYOrigin > 0) {
                                    isYIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    top: (isYIncreasing ? '-' : '+') + offsetY + 'px',
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px',
                                    width: (isYIncreasing ? '+' : '-') + offsetY * aspectxy + 'px',
                                });
                                break;
                            case direction.SE:
                                if (offsetYOrigin < 0) {
                                    isYIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px',
                                    width: (isYIncreasing ? '+' : '-') + offsetY * aspectxy + 'px',
                                });
                                break;
                        }
                        
                    }else{

                        switch (binding.value.direction) {
                            case direction.N:
                                if (offsetYOrigin > 0) {
                                    isYIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    top: (isYIncreasing ? '-' : '+') + offsetY + 'px',
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px'
                                });
                                break;
                            case direction.S:
                                if (offsetYOrigin < 0) {
                                    isYIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px'
                                });
                                break;
                            case direction.E:
                                if (offsetXOrigin < 0) {
                                    isXIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    width: (isXIncreasing ? '+' : '-') + offsetX + 'px'
                                });
                                break;
                            case direction.W:
                                if (offsetXOrigin > 0) {
                                    isXIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    width: (isXIncreasing ? '+' : '-') + offsetX + 'px',
                                    left: (isXIncreasing ? '-' : '+') + offsetX + 'px'
                                });
                                break;
                            case direction.NW:

                                if (offsetYOrigin > 0) {
                                    isYIncreasing = false;
                                }

                                if (offsetXOrigin > 0) {
                                    isXIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    width: (isXIncreasing ? '+' : '-') + offsetX + 'px',
                                    left: (isXIncreasing ? '-' : '+') + offsetX + 'px',
                                    top: (isYIncreasing ? '-' : '+') + offsetY + 'px',
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px'
                                });
                                break;
                            case direction.NE:
                                if (offsetYOrigin > 0) {
                                    isYIncreasing = false;
                                }
                                if (offsetXOrigin < 0) {
                                    isXIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    top: (isYIncreasing ? '-' : '+') + offsetY + 'px',
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px',
                                    width: (isXIncreasing ? '+' : '-') + offsetX + 'px'
                                });
                                break;
                            case direction.SE:

                                if (offsetYOrigin < 0) {
                                    isYIncreasing = false;
                                }

                                if (offsetXOrigin < 0) {
                                    isXIncreasing = false;
                                }

                                store.commit('setComponentStyle', {
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px',
                                    width: (isXIncreasing ? '+' : '-') + offsetX + 'px'
                                });
                                break;
                            case direction.SW:
                                if (offsetYOrigin < 0) {
                                    isYIncreasing = false;
                                }
                                if (offsetXOrigin > 0) {
                                    isXIncreasing = false;
                                }
                                store.commit('setComponentStyle', {
                                    height: (isYIncreasing ? '+' : '-') + offsetY + 'px',
                                    width: (isXIncreasing ? '+' : '-') + offsetX + 'px',
                                    left: (isXIncreasing ? '-' : '+') + offsetX + 'px'
                                });
                        }
                    }

                 }
                 counter ++;
        };

        el.ondragend = e => {
            console.log(e);
            return false;
        }
    }
});