/**
 * Created by chiqingzhen on 09/12/2016.
 */

import Vue from 'vue';
import store from '../store/index';




Vue.directive('dragMove', {
    inserted(el, binding) {

        el.setAttribute('draggable', true);

        var start = {
            x: 0,
            y: 0,
        };

        var count = 0;

        el.ondragstart = e => {
            start.x = e.pageX;
            start.y = e.pageY;
        };


        el.ondrag = function (e) {

            count ++;

            if (count %5 == 0) {

                var offsetYOrigin = e.pageY - start.y;
                var offsetXOrigin = e.pageX - start.x;
                start.x = e.pageX;
                start.y = e.pageY;
                var offsetY = Math.abs(offsetYOrigin);
                var offsetX = Math.abs(offsetXOrigin);
                var isXIncreasing = offsetXOrigin > 0;
                var isYIncreasing = offsetYOrigin > 0;


                if (offsetX > 40 || offsetY > 40) return;


                store.commit('setComponentStyle', {
                    top: (isYIncreasing ? '+': '-') + offsetY + 'px',
                    left: (isXIncreasing ? '+': '-') + offsetX + 'px',
                });

            }

            //监听是否接触边界
            store.commit("watchNearLine");

        }
    }
});