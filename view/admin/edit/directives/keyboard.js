/**
 * Created by chiqingzhen on 12/12/2016.
 */


import Vue from 'vue';
import store from '../store/index';


Vue.directive('keyboard', {
    inserted(el, binding) {
        el.setAttribute('tabIndex', 0);
        el.focus();
        el.onkeydown = function (e) {
            let cid = e.keyCode;
            var match = true;
            switch (cid) {
                case 8:
                    binding.value.keyDelete();
                    break;
                case 17 && 67:
                    binding.value.keyCopy();
                    break;
                case 17 && 86:
                    binding.value.keyPause();
                    break;
                case 38:
                    binding.value.upArrow();
                    break;
                case 40:
                    binding.value.downArrow();
                    break;
                case 37:
                    binding.value.leftArrow();
                    break;
                case 39:
                    binding.value.rightArrow();
                    break;
                default:
                    match = false;
                    break;
            }
            if (match) {
                e.preventDefault();
            }

        };
    }
});