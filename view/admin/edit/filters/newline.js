/**
 * Created by gaoying on 2016/12/26.
 */

import Vue from 'vue';

// Vue.filter('reverse', function (value) {
//     return value.split('').reverse().join('')
// });

Vue.filter('newline', function (value) {
    return value.replace(/<br\/>/g, "\n");
})