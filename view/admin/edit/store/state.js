/**
 * Created by chiqingzhen on 06/12/2016.
 */


export default {
    data: {
        front: {
            id: 'page-' + new Date().getTime() + Math.floor(Math.random() * 1000),
            selected: true,
            type: 'page',
            attr: {},
            style: {
                backgroundColor:'',
                backgroundImage: '',
                backgroundSize: 'contain',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                opacity: 100,
            },
            components: []
        },
        back: {
            id: 'page-' + new Date().getTime() + Math.floor(Math.random() * 1000),
            selected: true,
            type: 'page',
            attr: {},
            style: {
                backgroundColor: '',
                backgroundImage: '',
                backgroundSize: 'contain',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                opacity: 100,
            },
            components: [],
            animate:{
                animated:true,
                aniType:'none',
                duration:'1',
                delay:'1',
                time:'1',
                loop:''
            },
            other:{
                auto:false,
                spaceTime:'1s',
                groupNum:1,
                groupSpace:0
            },
        },
        pages: [{
            id: 'page-' + new Date().getTime() + Math.floor(Math.random() * 1000),
            selected: true,

            type: 'page',
            attr: {
                tplHeight:'627px',
                attrBtn:'static'
            },

            style: {
                backgroundColor:'transparent',
                backgroundImage: '',
                backgroundSize: 'contain',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                opacity: 100,
            },
            animate:{
                animated:true,
                aniType:'none',
                duration:'1',
                delay:'1',
                time:'1',
                loop:''
            },
            other:{
                auto:false,
                spaceTime:'1s',
                groupNum:1,
                groupSpace:0
            },
            components: []
        }],
        pageEffect:{
            dir:"vertical",
            effect:"slide",
            timer:"500",
            loop:'lyes',
            audioUrl:''
        },
        trackUrl:"",
        jsLink:""
    },
    status: {
        cateId:1,
        mediaId:1,
        tplType:1,
        selectType:'single',
        activePageIndex: 0,
        ComponentsMaxIndex:0,
        activeComponentsIndex:0,
        pageType: 'current',
        activeComponents: [{
            type: 'page',
            id:'',
            attr: {
                content:'',
                positionType:'top',
                linkType:'linkPage'
            },
            moved: false,
            selected: false,
            style: {
                fontSize:'14px',
                position: '',
                top: '',
                left: '',
                height: '',
                width: '',
                color:'#000',
                backgroundColor:"#000",
                fontFamily:"PingFangSC-Light",
                fontWeight:"",
                fontStyle:"",
                textDecoration:"",
                textAlign:"",
                lineHeight:"",
                opacity:"",
                borderStyle:"solid",
                borderWidth:"0px",
                borderColor:"#000",
                borderRadius:"",
                animationDuration:'1000ms',
                animationDelay:'0ms',
                animationIterationCount:1
            },
            animate:{
                animated:true,
                aniType:'none',
                duration:'1',
                delay:'1',
                time:'1',
                loop:''
            },
            other:{
                auto:false,
                spaceTime:'1s',
                isBottom:'',
                groupNum:1,
                groupSpace:0
            }
        }],
        clipboard:[],
        clipPageBoard:"",
        ctxMenu:{
            type:"main",
            display:"none",
            pointX:"220",
            pointY:"200"
        },
        border:{
            boardBorder:"board-border-none",
            center:"none",
            middle:"none"
        }
    }
}