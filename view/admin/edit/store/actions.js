/**
 * Created by chiqingzhen on 05/12/2016.
 */

// 这个操作使用 action 是因为包含两步骤：添加元素，和设置默认,加载对应的属性面板
export const addTextComponents = ({ commit, state, getters }, size) => {
    commit('cancelSelection');
    commit('addText', size);
    var currentPage = getters.currentPage;
    var component = currentPage.components[currentPage.components.length - 1];
    commit('setSelectedComponent', component);
};

export const addImageComponents = ({ commit, state, getters }, info) => {
    if (!info.path) return;
    commit('cancelSelection');
    commit('addImage', info);

    var currentPage = getters.currentPage;
    var component = currentPage.components[currentPage.components.length - 1];
    commit('setSelectedComponent', component);
};

export const addRectComponents = ({ commit, state, getters }, type) => {
    commit('cancelSelection');
    commit('addRect', type);

    var currentPage = getters.currentPage;
    var component = currentPage.components[currentPage.components.length - 1];
    commit('setSelectedComponent', component);
};

export const addPlugComponents = ({ commit, state, getters }, type) => {

    commit('cancelSelection');
    commit('addPlugs', type);

    var currentPage = getters.currentPage;
    var component = currentPage.components[currentPage.components.length - 1];
    commit('setSelectedComponent', component);
};


export const addFormComponents = ({ commit, state, getters }, type) => {

    commit('cancelSelection');
    commit('addForm', type);

    var currentPage = getters.currentPage;
    var component = currentPage.components[currentPage.components.length - 1];
    commit('setSelectedComponent', component);
};

export const deleteElement = ({ commit, state, getters }) => {
   commit('deleteElement');
};

export const copyElement = ({ commit, state, getters }) => {
    //添加当前元素到剪贴板
    commit('copyElement');
};

export const pauseElement = ({ commit, state, getters }) => {
    //新建与剪贴板元素相同的元素
    commit('pauseElement');
};

export const upArrow = ({ commit, state, getters }) => {
    //新建与剪贴板元素相同的元素
    commit('upArrow');
};

export const downArrow = ({ commit, state, getters }) => {
    //新建与剪贴板元素相同的元素
    commit('downArrow');
};

export const leftArrow = ({ commit, state, getters }) => {
    //新建与剪贴板元素相同的元素
    commit('leftArrow');
};

export const rightArrow = ({ commit, state, getters }) => {
    //新建与剪贴板元素相同的元素
    commit('rightArrow');
};