/**
 * Created by chiqingzhen on 05/12/2016.
 */

import eleattrs from './model/eleattrs';


export const fullState = state => state;
export const activeIndex = state => state.status.activePageIndex;
export const pages = state => state.data.pages;
export const currentPage = state => {
    if (state.status.pageType == 'current') {
        return state.data.pages[state.status.activePageIndex]
    } else {
        return state.data[state.status.pageType]
    }
};
export const frontPage = state => {
    return state.data['front'];
};

export const backPage = state => {
    return state.data['back'];
};



export const activeComponent = state => state.status.activeComponents;

export const ComponentsMaxIndex = state => state.status.ComponentsMaxIndex;
export const activeComponentsIndex = state => state.status.activeComponentsIndex;
export const ctxMenu = state => state.status.ctxMenu;
export const border = state => state.status.border;
export const tplHeight = state => state.data.pages[state.status.activePageIndex].attr.tplHeight;
export const tplType = state => state.status.tplType;
export const pageEffect = state => state.data.pageEffect;
export const cateId = state => state.status.cateId;
export const mediaId = state => state.status.mediaId;
export const selectType = state => state.status.selectType;
export const trackUrl = state => state.data.trackUrl;
export const jsLink = state => state.data.jsLink;

// 当前应该显示的属性列表， 分为 attr 和 style， 如果 存在不同类型，则取两种类型的子集
export const eleAttrs = state => {
    var components = state.status.activeComponents;
    if (components.length == 1) {
        return eleattrs[components[0].type||'text'];
    } else if (components.length >  1) {
        // 判断是否是同种控件
        if (new Set(components.map(com => com.type)).size > 1) {
            var i = 1;
            var attr = JSON.parse(JSON.stringify(eleattrs[components[0].type]));
            while (i < components.length) {

                if (Object.keys(components[i].styles ||{}).length == 0) {
                    Object.keys(attr.styles||{}).forEach(key => {
                        attr.styles[key] = false
                    })
                } else {
                    Object.keys(components[i].styles).forEach(key => {
                        attr.styles[key] = attr.styles[key] && (components[i].styles||{})[key];
                    });
                }

                if (Object.keys(components[i].edits ||{}).length == 0) {
                    Object.keys(attr.edits||{}).forEach(key => {
                        attr.edits[key] = false;
                    })
                } else {
                    Object.keys(components[i].edits).forEach(key => {
                        attr.edits[key] = attr.edits[key] && (components[i]||{}).edits[key];
                    });
                }
                i ++;
            }
            return attr;
        } else {
            return eleattrs[components[0].type];
        }

    } else {
        console.log(eleattrs[state.data.pages[state.status.activePageIndex].type]);
        return eleattrs[state.data.pages[state.status.activePageIndex].type]
    }
};