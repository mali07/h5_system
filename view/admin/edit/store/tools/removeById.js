/**
 * Created by gaoying on 16/12/16.
 *
 * 移除数组对象中指定属性指定值的数据，并返回后的数据
 *
 */
export default {

    "removeById":function(array,attr,val){

         var result=[];
         for(var index in array){
             console.log(index);
             if(array[index][attr]!=val){
                 result.push(array[index]);
             }else{
                 continue;
             }
         }
         return result;
     },

    //获取某属性元素在数组中的index值

    getIndexById:function(array,attr,val){
        for(var index in array){
            if(array[index][attr]==val){
                  return index;
            }
        }
        return -1;
    }
}