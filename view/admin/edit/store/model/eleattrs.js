/**
 * Created by gaoying on 16/12/8.
 */
export default {
    //文本类型
    text: {
        //编辑
        edits: {
            clickattr:true,
            content: true, //文本内容
            align: true,   //对齐方式
            fontFamily: true,  //字体类型
            fontSize:true, //字体大小
            bgColor:true,//背景颜色
            color:true,//字体颜色
            fontStyle:true,//文本样式
            lineHeight:true,//行高
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: true,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            padding: true
        },

        //动画
        animate: {
            turnpage:false,
            elementAni:true
        }
    },
    //图片
    image: {

        //编辑
        edits: {
            clickattr:true,
            imgsrc:true,
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: true,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:true,//阴影
            padding: true
        },
        //动画
        animate: {
            turnpage:false,
            elementAni:true
        }
    },

    //形状
    rect: {
        //编辑
        edits: {
            backpage:true,
            clickattr:true,
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: true,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:true,//阴影
            fillColor:true,
            padding: true
        },
        //动画
        animate: {
            turnpage:false,
            elementAni:true
        }
    },

    //轮播图插件
    swiper:{
        //编辑
        edits: {
            swiperImg:true
        },

        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: true,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:true,//阴影
            fillColor:true,
            padding: true
        },
        //动画
        animate: {
            turnpage:false,
            elementAni:false
        }
    },

    page: {
        edits: {
            backpage:true,
            bgColor: true,
            tplHeight:true,
            audioUrl:true,
            jsLink:true
        },
        styles: {
            opecity:true
        },
        //动画
        animate: {
            turnpage:true,
            elementAni:false
        }
    },

    shade:{
        edits: {
            backpage:true,
            bgColor: true,
            tplHeight:true,

        },
        styles: {
            opecity:true
        },
        //动画
        animate: {
            turnpage:true,
            elementAni:false
        }

    },

    card:{

        edits: {
            backpage:true,
            bgColor: true,
            formid:true,
            datasource:true
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:true,//阴影
            fillColor:true,
            padding: true
        },
        //动画
        animate: {
            turnpage:false,
            elementAni:false
        }
    },

    formarea:{

        //编辑
        edits: {
            backpage:true,
            clickattr:false,
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true
        },
        //动画
        animate: {
            turnpage:true,
            elementAni:false
        }
    },

    input:{

        //编辑
        edits: {
            backpage:true,
            clickattr:false,
            formid:true,
            placeholder:true,
            fontSize:true,
            lineHeight:true
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true
        },
        //动画
        animate: {
            turnpage:true,
            elementAni:false
        }
    },


    button:{

        //编辑
        edits: {
            buttontext:true,
            clickattr:false,
            buttontype:true,
            color:true,//字体颜色
            fontSize:true,
            lineHeight:true
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true
        },
        //动画
        animate: {
            turnpage:true,
            elementAni:false
        }
    },


    radio:{

        //编辑
        edits: {
            formid:true,
            clickattr:false,
            radioattr:true,
            color:true,//字体颜色
            fontSize:true//字体大小
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true,

        },
        //动画
        animate: {
            turnpage:true,
            elementAni:false
        }
    },


    checkbox:{

        //编辑
        edits: {
            formid:true,
            clickattr:false,
            radioattr:true,
            color:true,//字体颜色
            fontSize:true//字体大小
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true
        },
        //动画
        animate: {
            turnpage:true,
            elementAni:false
        }
    },
    textarea:{

        //编辑
        edits: {
            backpage:true,
            clickattr:false,
            formid:true,
            placeholder:true,
        },
        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true
        },
        //动画
        animate: {
            turnpage:true,
            elementAni:false
        }
    },
    scrollarea:{

        //编辑
        edits: {
            backpage:true,
            areacode:true
        },

        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true
        },
        //动画
        animate: {
            turnpage:false,
            elementAni:true
        }

    },

    dataContainer:{

        //编辑
        edits: {
            backpage:true,
            areacode:false,
            dataSource:true,
        },

        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true
        },
        //动画
        animate: {
            turnpage:false,
            elementAni:false
        }

    },

    dataItem:{
        //编辑
        edits: {
            cardType:true,
            color:true,//字体颜色
            nextIcon:true,
            itemNameId:true,
            itemLinkId:true,
            itemDetailId:true
        },

        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true
        },
        //动画
        animate: {
            turnpage:false,
            elementAni:false
        }

    },

    dataDetail:{
        //编辑
        edits: {
            color:true,//字体颜色
        },

        //外观
        styles: {
            foa:true,//布局坐标
            positionX:true,//X轴坐标
            positionY:true,//Y轴坐标
            width: true,//宽度
            height: true,//高度
            roted: false,//旋转
            opecity:true,//透明度
            borderStyle:true,//边框样式
            borderWidth:true,//边框宽度
            borderColor:true,//边框颜色
            borderRadius:true,//圆角
            shadow:false,//阴影
            fillColor:false,
            padding: true
        },
        //动画
        animate: {
            turnpage:false,
            elementAni:false
        }

    },

}