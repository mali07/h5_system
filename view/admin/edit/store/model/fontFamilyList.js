/**
 * Created by gaoying on 16/12/12.
 */
export default{

    fontFamilyList: [
        { text: 'PingFangSC-Light', value: 'PingFangSC-Light' },
        { text: 'sans-serif', value: 'sans-serif' },
        { text: 'serif', value: 'serif' },
        { text: 'monospace', value: 'monospace' },
        { text: 'fantasy', value: 'fantasy' },
        { text: 'cuisive', value: 'cuisive' },
        { text: 'Helvetica, sans-serif', value: 'Helvetica, sans-serif' },
        { text: 'Arial, sans-serif', value: 'Arial, sans-serif' },
        { text: 'Verdana,sans-serif', value: 'Verdana,sans-serif' },
        { text: 'Tahoma, sans-serif', value: 'Tahoma, sans-serif' },
        { text: 'Georgia, serif', value: 'Georgia, serif' },
        { text: 'Times, serif', value: 'Times, serif' },
        { text: '黑体', value: 'SimHei' },
        { text: '宋体', value: 'SimSun' },
        { text: '新宋体', value: 'NSimSun' },
        { text: '仿宋', value: 'FangSong' },
        { text: '楷体', value: 'KaiTi' },
        { text: '微软雅黑', value: 'Microsoft YaHei' },
        { text: '隶书', value: 'LiSu' },
        { text: '幼圆', value: 'YouYuan' },
        { text: '华文细黑', value: 'STXihei' },
        { text: '华文楷体', value: 'STKaiti' },
        { text: '华文宋体', value: 'STSong' },
    ]

}