/**
 * Created by gaoying on 16/12/12.
 */
export default{

    borderStyleList: [
        { text: 'none', value: 'none' },
        { text: 'hidden', value: 'hidden' },
        { text: 'dotted', value: 'dotted' },
        { text: 'dashed', value: 'dashed' },
        { text: 'solid	', value: 'solid' },
        { text: 'double', value: 'double' },
        { text: 'groove', value: 'groove' },
        { text: 'ridge', value: 'ridge' },
        { text: 'inset', value: 'inset' },
        { text: 'outset', value: 'outset' },
        { text: 'inherit', value: 'inherit' },
    ]
}