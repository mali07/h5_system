/**
 * Created by chiqingzhen on 06/12/2016.
 */


import { pageAdapter } from './index'

var typeMap = {
    type1: {
        backgroundColor: '#414146',
        borderWidth: '0'
    },
    type2: {
        backgroundColor: '#414146',
        borderWidth: '0',
        borderRadius: '10px'
    },
    type3: {
        backgroundColor: '#FFFFFF',
        borderWidth: '2px',
        borderColor: '#000000'
    },
    type4: {
        backgroundColor: '#414146',
        borderRadius: '50%'
    },
    type5: {
        backgroundColor: '#414146',
        borderRadius: '50%',
        borderWidth: '2px'
    },
    type6: {
        borderRadius: '50%',
        borderWidth: '2px'
    }

};


export const addRect = (state, type) => {
    var comIndex = state.status.ComponentsMaxIndex;
    var text = {
        type: 'rect',
        id: new Date().getTime(),
        attr: {
            position:'absolute',
            positionType:'top',
            attrBtn:'static',    //static 静态 button 可点击，可设置事件
            linkType:'none',  //linkUrl外部跳转  linkPage内部页面跳转
            linkUrl:'#',  //跳转路径
            linkNum:'1',  //跳转页面序号
            clickLog:'' //埋点参数
        },
        moved: false,
        selected: false,
        style: {
            position: 'absolute',
            top: '70px',
            left: '70px',
            height: '70px',
            width: '70px',
            color: '#000',
            padding: 0,
            backgroundColor: "#fff",
            transform:'',
            transformRotate:0,
            transformOrigin:"50% 50%",
            opacity: "",
            borderStyle:"solid",
            borderWidth:"0px",
            borderColor:"#000",
            borderRadius: "0px",
            animationDuration: "1000ms",
            animationDelay:"0ms",
            animationIterationCount:1,
            zIndex:300+comIndex,
            ...typeMap[type]
        },
        animate:{
            animated:true,
            aniType:'none',
            duration:'1',
            delay:'1',
            time:'1',
            loop:''
        },
        other:{
            auto:false,
            spaceTime:'1s',
            isBottom:''
        },
    };

    // 查找是否存在插入后未曾移动过位置的元素，如果存在，新插入的元素做一定量的偏移，防止重叠
    var noMove = pageAdapter(state).components.filter(text => text.moved == false);
    if (noMove.length > 0) {
        text.style.top = (parseInt(noMove[noMove.length - 1].style.top.split('p')[0]) + 30) + 'px';
        text.style.left = (parseInt(noMove[noMove.length - 1].style.left.split('p')[0]) + 20) + 'px';
    }
    state.status.ComponentsMaxIndex++;
    pageAdapter(state).components.push(text);

};
