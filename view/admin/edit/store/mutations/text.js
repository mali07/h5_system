/**
 * Created by chiqingzhen on 06/12/2016.
 */

import * as FONTSIZE from '../types/text';
import {pageAdapter} from './index'

export const addText = (state, size) => {
    var fontSize = '14px';
    var fontText = '小号文字';
    if (size == FONTSIZE.SMALL) {
        fontSize = '14px';
        fontText = '小号文字'
    }
    if (size == FONTSIZE.MIDDLE) {
        fontSize = '18px';
        fontText = '中等文字';
    }
    if (size == FONTSIZE.BIG) {
        fontSize = '22px';
        fontText = '大号文字';
    }
    var comIndex = state.status.ComponentsMaxIndex;
    var text = {
        type: 'text',
        id: new Date().getTime(),
        attr: {
            position:'absolute',
            positionType:'top',
            content: fontText,
            attrBtn:'static',    //static 静态 button 可点击，可设置事件
            linkType:'none',  //linkUrl外部跳转  linkPage内部页面跳转
            linkUrl:'#',  //跳转路径
            linkNum:'1',   //跳转页面序号
            clickLog:'' //埋点参数
        },
        moved: false,
        selected: false,
        style: {
            fontSize: fontSize,
            position: 'absolute',
            top: '40px',
            left: '40px',
            height: '40px',
            width: '100px',
            color: '#000',
            padding: 0,
            backgroundColor: "transparent",
            backgroundImage: '',
            backgroundSize: 'contain',
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',
            fontFamily: "PingFangSC-Light",
            fontWeight: "",
            fontStyle: "",
            textDecoration: "",
            textAlign: "",
            lineHeight: "20px",
            transform:'',
            transformRotate:0,
            transformOrigin:"50% 50%",
            opacity: 1,
            borderStyle:"solid",
            borderWidth:"0px",
            borderColor:"#000",
            borderRadius: "0px",
            whiteSpace: 'pre-line',
            animationDuration: "1000ms",
            animationDelay:"0ms",
            animationIterationCount:1,
            zIndex:300+comIndex,
        },
        animate:{
            animated:true,
            aniType:'none',
            duration:'1',
            delay:'1',
            time:'1',
            loop:''
        },
        other:{
            auto:false,
            spaceTime:'1s',
            isBottom:''
        }
    };
    // 查找是否存在插入后未曾移动过位置的元素，如果存在，新插入的元素做一定量的偏移，防止重叠
    var noMove = pageAdapter(state).components.filter(text => text.moved == false);
    if (noMove.length > 0) {
        text.style.top = (parseInt(noMove[noMove.length - 1].style.top.split('p')[0]) + 30) + 'px';
        text.style.left = (parseInt(noMove[noMove.length - 1].style.left.split('p')[0]) + 20) + 'px';
    }
    state.status.ComponentsMaxIndex++;
    pageAdapter(state).components.push(text);

};
