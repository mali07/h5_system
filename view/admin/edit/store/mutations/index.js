/**
 * Created by chiqingzhen on 05/12/2016.
 */


import {addText} from './text';
import {addImage} from './image';
import {addRect} from  './rect';
import {addPlugs} from './plugs';
import {addForm} from './form';
import tools from '../tools/removeById';


export const pageAdapter = state => {
    if (state.status.pageType == 'current') {
        return state.data.pages[state.status.activePageIndex]
    } else {
        return state.data[state.status.pageType]
    }
};

export default {
    /*
    * 设置编辑元素位置,已点中元素为基准
    * **/
    setGroupPosition(state,param){
        var activeComponents = state.status.activeComponents;
        var type=param.key;
        var value = parseInt(param.value);

        var flag= parseInt(activeComponents[0].style[type]);
        var changevalue = value-flag;
        for(var i in activeComponents){
            activeComponents[i].style[type] = parseInt(activeComponents[i].style[type])+changevalue + "px";
        }
    },

    /*
    * 等比设置宽高
    * **/
    setWH(state,param){
        var widthsrc = parseInt(state.status.activeComponents[0].style.width);
        var heightsrc = parseInt(state.status.activeComponents[0].style.height);

        if(param.key=='style.width'){
            var width =parseInt(param.value);
            var height = parseInt(heightsrc/widthsrc * width);
            state.status.activeComponents[0].style.width = width + "px" ;
            state.status.activeComponents[0].style.height = height + "px" ;
        }

        if(param.key=='style.height'){
            var height =parseInt(param.value);
            var width = parseInt(widthsrc/heightsrc * height);
            state.status.activeComponents[0].style.width = width + "px" ;
            state.status.activeComponents[0].style.height = height + "px" ;
        }

    },
    /*
    * 编组操作
    * **/
    group(state){
        var components = state.status.activeComponents;
        var id = new Date().getTime();
        for(var i in components){
            components[i].id = id;
        }
        state.status.ctxMenu.display="none";
    },

    /*
     * 解组操作
     * **/
    ungroup(state){
        var components= state.status.activeComponents;
        for(var i in components){
            var id = new Date().getTime()+i;
            components[i].id = id;
        }
        state.status.ctxMenu.display="none";
    },

    /*
    * 设置多选元素对齐方式,均已第一个元素为参照
    * **/
    setComAlign(state,type){


        var activeComponents =  state.status.activeComponents;

        var referStyle =activeComponents[0].style;
        var referleft =  parseInt(referStyle.left);
        var refertop =  parseInt(referStyle.top);

        var centerx = referleft+ parseInt(referStyle.width)/2;
        var centery = refertop + parseInt(referStyle.height)/2;

        var rightx= referleft+ parseInt(referStyle.width);
        var bottomy = refertop + parseInt(referStyle.height);



        for(var i in activeComponents ){

            switch(type){
                case 'left' :
                    activeComponents[i].style.left = referleft + "px";
                    break;
                case 'center' :
                    activeComponents[i].style.left = centerx -  parseInt(activeComponents[i].style.width)/2 + "px";
                    break;
                case 'right' :
                    activeComponents[i].style.left = rightx - parseInt(activeComponents[i].style.width) + "px";
                    break;
                case 'top' :
                    activeComponents[i].style.top = refertop + "px";
                    break;
                case 'vertical' :
                    activeComponents[i].style.top = centery -  parseInt(activeComponents[i].style.height)/2 + "px";
                    break;
                case 'bottom' :
                    activeComponents[i].style.top = bottomy - parseInt(activeComponents[i].style.height) + "px";
                    break;
            }
        }
    },

    /*
     * 单选按钮组或多选择按钮组添加新选项
     * */
    addNewSelectItem(state,index){
        var srclist=state.status.activeComponents[0].attr.items;
        var groupid=state.status.activeComponents[0].attr.id;
        var item={
           id:groupid+"_"+(index+1),
           text:"选项描述"
        }
        state.status.activeComponents[0].attr.items=srclist.slice(0,index+1).concat(item).concat(srclist.slice(index+1));
    },

    /*
     * 单选按钮组或多选择按钮组删除某选项
     * */
    minusSelectItem(state,index){
        var srclist=state.status.activeComponents[0].attr.items;
        state.status.activeComponents[0].attr.items=srclist.slice(0,index).concat(srclist.slice(index+1));
    },
    /*
    * 设置当前操作的cateId
    *
    **/
    setCateId(state,cateid){
        state.status.cateId=cateid;
    },


    /*
     * 设置当前操作的mediaId
     *
     **/
    setMediaId(state,mediaid){
        state.status.mediaId=mediaid;
    },

    /*
    *  删除一张轮播图片
    *
    * **/
    minusSwiperImg(state,index){
        var srclist=state.status.activeComponents[0].attr.src;
        state.status.activeComponents[0].attr.src=srclist.slice(0,index).concat(srclist.slice(index+1));
    },
    /*
     * 添加轮播图片
     *
     * */
    addNewSwiperImg(state,index){
        var srclist=state.status.activeComponents[0].attr.src;
        var defaulturl="/static/img/seat.jpg";
        state.status.activeComponents[0].attr.src=srclist.slice(0,index+1).concat(defaulturl).concat(srclist.slice(index+1))
    },

    /*
    *  设置轮播图图片路径
    * */
    setSwiperImg(state,param){
        var url=param.src.split("\"")[1];
        var index=param.index;
        state.status.activeComponents[0].attr.src[index]=url;
    },

    /*
    *  设置轮播图属性
    *
    * **/
    setSwiperCtrl(state,param){

        // 如果是分组数，则需要重新计算
        if(param.key=="groupNum"){
            state.status.activeComponents[0].other['sliderWidth']=375/param.value + "px";
        }
        if(param.key=="groupSpace"){
            state.status.activeComponents[0].other['sliderMargin']=param.value+"px";
        }

        state.status.activeComponents[0].other[param.key]=param.value;

    },

    /*
    *  操控台播放动画
    * */
    playAnimate(state){
       var anitype = state.status.activeComponents[0].animate.aniType;
       state.status.activeComponents[0].animate.aniType='none';
       setTimeout(function(){
           state.status.activeComponents[0].animate.aniType=anitype;
       },500)

    },

    /*
    *  设置翻页动画  setPageEffect
    *  @param key :属性名
    *         value : 属性值
    * */
    setPE(state,param){
        state.data.pageEffect[param.key]=param.value;
    },

    //设置trackLogUrl

    setTU(state,trackurl){
        state.data.trackUrl=trackurl;
    },
    setJsLink(state,jsurl){
        state.data.jsLink=jsurl;
    },
    
    /*
    *  设置元素动画 setElementAnimate,支持组元素操作
    *  @param key :属性名
    *        value : 属性值
    *
    * */
    setEA(state,param){
        var activeComponents =  state.status.activeComponents;
        for(var i in activeComponents){
            activeComponents[i].animate[param.key]=param.value;
        }
    },

    //设置模板高度
    setTplHeight(state,height){
        var tplHeight=height+"px";
        var pageindex=state.status.activePageIndex;
        state.data.pages[pageindex].attr.tplHeight=tplHeight;
    },

    //设置模板类型
    setTplType(state,pt){
        state.status.tplType=pt;
    },

    //初始化board信息
    setBoardInfo(state,info){
        state.data.boardInfo.lineLeft=info.lineLeft;
        state.data.boardInfo.lineRight=info.lineRight;
        state.data.boardInfo.lineTop=info.lineTop;
        state.data.boardInfo.lineBottom=info.lineBottom;
        state.data.boardInfo.lineCenter=info.lineCenter;
        state.data.boardInfo.lineMiddle=info.lineMiddle;
    },

    // 设定当前选中的页面
    setActiveIndex(state, index) {
        state.status.activePageIndex = index
    },

    setPageType(state, type) {
        state.status.pageType = type;
    },

    // 增加一页
    addPage(state,pagetype) {
        state.data.pages.push({
            id: pagetype+'-' + new Date().getTime(),
            selected: true,
            type: pagetype,
            attr: {
                tplHeight:'627px',
                attrBtn:'static'
            },
            style: {
                backgroundColor:'transparent',
                backgroundImage: '',
                backgroundSize: 'contain',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                opacity: 100,
            },
            components: [],
            animate:{
                animated:true,
                aniType:'none',
                duration:'1',
                delay:'1',
                time:'1',
                loop:''
            },
            other:{
                auto:false,
                spaceTime:'1s',
                groupNum:1,
                groupSpace:0
            },
        });
        state.status.ComponentsMaxIndex=0;
    },

    // 增加一个文字控件
    addText,
    // 增加一张图片
    addImage,
    addRect,
    addPlugs,
    addForm,
    // 选中一个控件,如果是组元素，选中整个组元素（id相同的元素）
    setSelectedComponent(state, component) {
        state.status.selectType = 'single';
        state.status.activeComponents.forEach(function (c) {
            c.selected = false;
        });
        var id=component.id;

        state.status.activeComponents = [];
        state.status.activeComponents.push(component);

        var components = pageAdapter(state).components;
        for (var i in components){
            if(components[i].id == id && components[i]!=component){
                state.status.activeComponents.push(components[i]);
            }
        }
        state.status.activeComponents.forEach(function(c) {
            c.selected = true;
        });
        if(state.status.activeComponents.length > 1){
            state.status.selectType = 'multi';
        }
    },
    cancelSelectComponent(state) {
        state.status.activeComponents.forEach(comp => {
            comp.selected = false
        });
        state.status.activeComponents = [];
    },
    //多选
    addSelectedComponent(state, component) {

        state.status.selectType = 'multi';


        state.status.activeComponents.push(component);

        state.status.activeComponents.forEach(function (c) {
            c.selected = true;
        });
    },

    // 设定当前选中控件的样式
    setComponentStylea(state, styleObject) {
        // 如果提交过来的对象包含多个属性，则一起进行提交
        for (var i in styleObject) {
            if (styleObject.hasOwnProperty(i)) {
                var newValue = '';
                if (/^[\+\-]/.test(styleObject[i])) {
                    var method = styleObject[i].charAt(0);
                    var amount = parseInt(/(\d+)/.exec(styleObject[i])[0]);
                    state.status.activeComponents.forEach(function (c) {
                        if (method == '+') {
                            newValue = (parseInt(c.style[i].split('px')[0]) + amount) + 'px';
                        } else {
                            newValue = (parseInt(c.style[i].split('px')[0]) - amount) + 'px';
                        }
                    });
                } else {
                    newValue = styleObject[i];
                }
                state.status.activeComponents.forEach(function (c) {
                    c.style[i] = newValue;
                })
            }
        }
    },
    setComponentStyle(state, styleObject) {

        function set(key, value, comp) {
            var newValue = ''
            if (/^[\+\-]/.test(value)) {
                var method = value.charAt(0);
                var amount = parseInt(/(\d+)/.exec(value)[0]);
                if (method == '+') {
                    newValue = (parseInt(comp.style[key].split('px')[0]) + amount) + 'px'
                } else {
                    newValue = (parseInt(comp.style[key].split('px')[0]) - amount) + 'px'
                }
            } else {
                newValue = value;
            }
            comp.style[key] = newValue;
        }

        Object.keys(styleObject).forEach(key => {
            if (state.status.activeComponents.length > 0) {
                state.status.activeComponents.forEach(comp => {
                    set(key, styleObject[key], comp)
                })
            } else {
                set(key, styleObject[key], pageAdapter(state))
            }
        });

    },
    setAttribute(state, attr) {
        function set(key, value, comp) {
            var keys = key.split('.');
            while (keys.length > 1) {
                comp = comp[keys[0]];
                keys.shift();
            }

            if (keys.length == 1) {
                comp[keys[0]] = value;
                if(keys[0]=="animationDuration"||keys[0]=="animationDelay"){
                    comp[keys[0]]=parseInt(value)+"ms";
                }
            }
        }

        Object.keys(attr).forEach(function (key) {
            if (state.status.activeComponents.length > 0) {
                state.status.activeComponents.forEach(comp => {
                    set(key, attr[key], comp)
                })
            } else {
                set(key, attr[key], pageAdapter(state))
            }
        })
    },
    //设置文本内容
    setTextContent(state, content){
        state.status.activeComponents.forEach(function (c) {
            c.attr.content = content;
        })
    },
    // 取消选中所有选中控件
    cancelSelection(state) {
        if (Array.isArray(state.status.activeComponents)) {
            state.status.activeComponents.forEach(function (c) {
                c.selected = false;
            })
        }
    },

    // 删除当前页选中元素,支持多选元素及编组元素删除
    deleteElement(state){

        var activeComponents=state.status.activeComponents;
        if (activeComponents.length != 0) {
            for(var i in activeComponents){
                var id = state.status.activeComponents[i].id;
                pageAdapter(state).components = tools.removeById(pageAdapter(state).components, "id", id);
            }
        }
        state.status.ctxMenu.display="none";
    },

    //删除背景图
    deleteBackground(state){

        if(state.data.pages[state.status.activePageIndex].style.backgroundImage!=''){

            //删除当前背景
            state.data.pages[state.status.activePageIndex].style.backgroundImage='';
        }else{
            //删除全局背景
            state.data.back.style.backgroundImage='';
        }
        state.status.ctxMenu.display="none";
    },

    //删除当前选中页
    deletePage(state){

        var index=state.status.activePageIndex;
        //如果是最后一页
        if(state.data.pages.length==1){
            state.status.ctxMenu.display="none";
            return;
        }
        if(index==state.data.pages.length-1){
            state.status.activePageIndex=index-1;
        }
        state.data.pages=state.data.pages.splice(0,index).concat(state.data.pages.splice(1));
        state.status.ctxMenu.display="none";


    },
    //置于上一层
    upLayer(state){

       var pageIndex = state.status.activePageIndex;
       var currentComId = state.status.activeComponents[0].id;
       var currentIndex = tools.getIndexById(state.data.pages[pageIndex].components,'id',currentComId);
       state.data.pages[pageIndex].components[currentIndex].style.zIndex += 1;
       state.data.pages[pageIndex].components[currentIndex+1].style.zIndex -= 1;

       state.status.ctxMenu.display="none";
    },
    //置于下一层
    downLayer(state){

        var pageIndex = state.status.activePageIndex;
        var currentComId = state.status.activeComponents[0].id;
        var currentIndex = tools.getIndexById(state.data.pages[pageIndex].components,'id',currentComId);
        state.data.pages[pageIndex].components[currentIndex].style.zIndex -= 1;
        state.data.pages[pageIndex].components[currentIndex-1].style.zIndex += 1;

        state.status.ctxMenu.display="none";
    },

    //复制当前页到剪贴板
    copyPage(state){
        state.status.clipPageBoard=state.status.activePageIndex;
        state.status.ctxMenu.display="none";
    },
    //粘贴当前页
    pausePage(state){

        var index=state.status.clipPageBoard;
        state.data.pages=state.data.pages.slice(0,index).concat(state.data.pages[index]).concat(state.data.pages.slice(index));
        state.status.activePageIndex+=1;
        state.status.ctxMenu.display="none";
    },


    //复制到剪贴板
    copyElement(state){
        state.status.clipboard = state.status.activeComponents;
        state.status.ctxMenu.display="none";
    },

    //粘贴,copy之后的元素被选中
    pauseElement(state){

        var clipboardelement =JSON.parse(JSON.stringify(state.status.clipboard));
        if (Array.isArray(pageAdapter(state).components)) {
            pageAdapter(state).components.forEach(function (c) {
                c.selected = false;
            })
        }
        state.status.activeComponents =[];
        for(var i in clipboardelement){
            var tempelement = clipboardelement[i];
            tempelement.id = new Date().getTime();
            clipboardelement[i].moved = false;
            pageAdapter(state).components.push(clipboardelement[i]);
            state.status.activeComponents.push(clipboardelement[i])
        }

        state.status.ctxMenu.display="none";

    },

    //向上移动元素，多选中元素操作有效
    upArrow(state){
        var activeComponents = state.status.activeComponents;
        for(var i in activeComponents){
            state.status.activeComponents[i].style.top = parseInt(state.status.activeComponents[i].style.top) - 1 + "px";
        }
    },

    //向下移动元素
    downArrow(state){
        var activeComponents = state.status.activeComponents;
        for(var i in activeComponents){
            state.status.activeComponents[i].style.top = parseInt(state.status.activeComponents[i].style.top) + 1 + "px";
        }
    },

    //向左移动元素
    leftArrow(state){
        var activeComponents = state.status.activeComponents;
        for(var i in activeComponents){
            state.status.activeComponents[i].style.left = parseInt(state.status.activeComponents[i].style.left) - 1 + "px";
        }
    },

    //向右移动元素
    rightArrow(state){
        var activeComponents = state.status.activeComponents;
        for(var i in activeComponents){
            state.status.activeComponents[i].style.left = parseInt(state.status.activeComponents[i].style.left) + 1 + "px";
        }
    },

    //鼠标右键弹出菜单
    showCtxMenu(state,param){



        state.status.ctxMenu.type=param.type;

        if(param.type=="main"){
            //如果没有焦点元素，则全部置灰
            if(state.status.activeComponents.length==0){
                state.status.ctxMenu.type="default";
            }
        };
        state.status.ctxMenu.display=param.display;
        state.status.ctxMenu.pointX=param.pointX;
        state.status.ctxMenu.pointY=param.pointY;
    },
    //监控是否贴边，贴底边时清除top属性，用bottom属性
    watchNearLine(state){

        if(state.status.selectType == 'single'){

            var currentElement=state.status.activeComponents[0];
            var border=state.status.border;
            var pointX=parseInt(currentElement.style.left);
            var pointY=parseInt(currentElement.style.top);
            var elewidth=parseInt(currentElement.style.width);
            var eleheight=parseInt(currentElement.style.height);

            var dvleft=Math.abs(pointX-0);
            var dvright=Math.abs(pointX+elewidth-375);
            var dvtop=Math.abs(pointY-0);
            var dvbottom=Math.abs(pointY+eleheight-627);
            var dvcenter =Math.abs(pointX+elewidth/2-187);
            var dvmiddle=Math.abs(pointY+eleheight/2-325);


            if(dvleft <= 5){
                currentElement.style.left=0+"px";
                border.boardBorder="board-border-left";
            }else{
                border.boardBorder="board-border-none";
            }
            if(dvright <= 5){
                currentElement.style.left=375 - elewidth + "px";
                border.boardBorder="board-border-right";
            }else{
                border.boardBorder="board-border-none";
            }
            if(dvtop <=5){
                currentElement.style.top=0+"px";
                border.boardBorder="board-border-top";
            }else{
                border.boardBorder="board-border-none";
            }
            if(dvbottom <= 5){
                currentElement.style.top=627 - eleheight +"px";
                currentElement.other.isBottom='bottom';
                border.boardBorder="board-border-bottom";
            }else{
                currentElement.other.isBottom='';
                border.boardBorder="board-border-none";
            }
            if(dvcenter <= 5){
                currentElement.style.left=187 - elewidth/2 +"px";
                border.center="block";
            }else{
                border.center="none";
            }
            if(dvmiddle <= 5){
                currentElement.style.top=325 - eleheight/2 +"px";
                border.middle="block";
            }else{
                border.middle="none";
            }


        }



    }


}