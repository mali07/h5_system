/**
 * Created by gaoying on 2017/6/5.
 */


import { pageAdapter } from './index'

var typeMap = {
    //表单域
    formarea: {
        style:{
            backgroundColor: '#414146',
            height: '200px',
            width: '300px',
        }

    },

    input: {
        attr:{
            id:"",
            placeholder:""
        },
        style:{

            backgroundColor: '#fff',
            height: '30px',
            width: '200px',
            borderRadius: "5px",
            textAlign:"left",
            border:"1px solid #767676",
            paddingLeft:"10px",
            fontSize:"14px",
            lineHeight:"30px",
            outline:"none",
            cursor:"pointer"
        }

    },

    button:{
        attr:{
            id:"",
            buttontext:"提交",
            type:"submit",
            reqMethod:"get",
            reqUrl:"",
            clickLog:'' //埋点参数
        },
        style:{
            backgroundColor: '#fd6565',
            height: '30px',
            width: '200px',
            borderRadius: "5px",
            textAlign:"center",
            color:"#fff",
            lineHeight:"30px",
            fontSize:"14px",
            border:"none",
            outline:"none",
            cursor:"pointer"
        }
    },

    radio:{
        attr:{
            id:"rd",
            items:[
                {
                    text:"选项描述"
                }
            ]
        },
        style:{
            height: '30px',
            width: '200px',
            background:'rgba(0,0,0,0)',
            fontSize:'14px',
        }
    },

    checkbox:{

            attr:{
                id:"ck",
                items:[
                    {
                        text:"选项描述"
                    }
                ]
            },

            style:{
                height: '30px',
                width: '200px',
                background:'rgba(0,0,0,0)',
                fontSize:'14px',
            }
    },

    textarea:{
        attr:{
            id:"",
            placeholder:""
        },
        style:{
            height: '100px',
            width: '200px',
            background:'#fff',
            padding:"5px"
        }
    }

};


export const addForm = (state, type) => {

    var text = {
        type: type,
        id: new Date().getTime(),
        attr: {
            positionType:'top',
            ...typeMap[type].attr
        },
        moved: false,
        selected: false,
        style: {
            position: 'absolute',
            top: '70px',
            left: '70px',
            color: '#000',
            padding: 0,
            backgroundColor: "#fff",
            transfrom: "",
            opacity: "",
            ...typeMap[type].style
        },
        animate:{
            animated:true,
            aniType:'none',
            duration:'1',
            delay:'1',
            time:'1',
            loop:''
        },
        other:{
            auto:false,
            spaceTime:'1s',
            isBottom:''
        },
    };

    // 查找是否存在插入后未曾移动过位置的元素，如果存在，新插入的元素做一定量的偏移，防止重叠
    var noMove = pageAdapter(state).components.filter(text => text.moved == false);
    if (noMove.length > 0) {
        text.style.top = (parseInt(noMove[noMove.length - 1].style.top.split('p')[0]) + 30) + 'px';
        text.style.left = (parseInt(noMove[noMove.length - 1].style.left.split('p')[0]) + 20) + 'px';
    }

    pageAdapter(state).components.push(text);

};

