/**
 * Created by gaoying on 2017/3/30.
 */

import { pageAdapter } from './index'


var typeMap = {

    //轮播图
    swiper:{
        attr:{

        },
        style:{
            width:'375px',
            height:'200px',
        },
        swiperCtrl:{
            auto:false,
            spaceTime:'5000',
            groupNum:1,
            groupSpace:0,
            sliderWidth:'375px',
            sliderMargin:'0px'
        }
    },

    shade:{
    },

    card:{
        style:{
            width:'300px',
            height:'200px',
            backgroundColor:'#414146'
        },
        attr:{
            id:'',
            dataSource:''
        }

    },

    scrollarea:{
        style:{
            width:'300px',
            height:'200px',
            overflowX:'hidden',
            overflowY:'auto',
            borderStyle: "solid",
            borderWidth: "1px",
            borderColor: "#fff",
        },
        attr:{
            id:'',
            areacode:'',
            noswiper:'swiper-no-swiping'
        }
    },

    dataContainer:{
        style:{
            width:'300px',
            height:'200px',
            overflowX:'hidden',
            overflowY:'hidden',
            borderStyle: "solid",
            borderWidth: "1px",
            borderColor: "#fff",
        },
        attr:{
            id:'',
            dataSource:'',
            noswiper:'swiper-no-swiping'
        }
    },

    dataItem:{
        style:{
            width:'280px',
            height:'40px',
            backgroundColor:'#414146',
            color:'#fff'
        },
        attr:{
            id:'',
            cardType:"ct01", //ct01 点击跳转  ，ct02点击展开
            nextIcon:'',
            itemNameId:'',
            itemLinkId:'',
            itemDetailId:'',
        }

    },

    dataDetail:{
        style:{
            width:'280px',
            height:'200px',
            backgroundColor:'#414146',
            color:'#fff'
        },
        attr:{
            id:'',
        }
    }

};


export const addPlugs = (state, type) => {
    var comIndex = state.status.ComponentsMaxIndex;
    var plug = {
        type:type,
        id: new Date().getTime(),
        attr: {
            positionType:'top',
            src:[
                '/static/img/seat.jpg'
            ],
            ...typeMap[type].attr
        },
        moved: false,
        selected: false,
        style: {
            position: 'absolute',
            top: '0px',
            left: '0px',
            height: '70px',
            width: '70px',
            color: '#000',
            padding: 0,
            backgroundColor: "",
            fontFamily: "",
            fontWeight: "",
            fontStyle: "",
            textDecoration: "",
            textAlign: "",
            lineHeight: "",
            transfrom: "",
            opacity: "",
            borderStyle: "",
            borderWidth: "",
            borderColor: "",
            borderRadius: "",
            animationDuration: "1s",
            animationDelay:"0s",
            animationIterationCount:1,
            zIndex:300+comIndex,
            ...typeMap[type].style
        },
        other:{
            isBottom:'',
            ...typeMap[type].swiperCtrl
        },
        animate:{
            animated:true,
            aniType:'none',
            duration:'1',
            delay:'1',
            time:'1',
            loop:''
        }
    };

    debugger

    // 查找是否存在插入后未曾移动过位置的元素，如果存在，新插入的元素做一定量的偏移，防止重叠
    var noMove = pageAdapter(state).components.filter(text => text.moved == false);
    if (noMove.length > 0) {
        plug.style.top = (parseInt(noMove[noMove.length - 1].style.top.split('p')[0]) + 30) + 'px';
        plug.style.left = (parseInt(noMove[noMove.length - 1].style.left.split('p')[0]) + 20) + 'px';
    }
    pageAdapter(state).components.push(plug);
};
