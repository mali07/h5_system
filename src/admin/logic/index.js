'use strict';
// import uuid from 'node-uuid';
/**
 * logic
 * @param  {} []
 * @return {}     []
 */
export default class extends think.logic.base {
  /**
   * index action logic
   * @return {} []
   */
  async indexAction(self){

      var ticket = this.cookie("sso-ticket");
      var userinfo = JSON.parse(await think.cache(ticket,undefined,{type: 'redis'}));
      var name = userinfo.userName;
      var usermodel = this.model('users');

      //查看是否是新用户
      try{
          var isnew = await usermodel.where({oaname: {'=': name }}).select();

          if(isnew.length > 0 && isnew[0].oaname==name){

              await this.session('userId',isnew[0].id);
              await this.session('role',isnew[0].role);
              self.assign('role',isnew[0].role);

          }else{
              var id= await usermodel.add({
                  oaname: name,
                  name:userinfo.realName
              });
              await this.session('userId',id);
              await this.session('role','2');
              self.assign('role','2');
          }
          await this.session('userName',name);
          await this.session('realName',userinfo.realName);
          self.assign('realName',userinfo.realName);
          return true;
      }catch(e){
          return this.json({code: 500});
      }
    }

    async listAction(self){
        let realName =await this.session("realName");
        let role =await this.session("role");
        self.assign('role',role);
        self.assign('realName',realName);

    }

    //用户作品
    async innovateAction(self){
        let realName =  await this.session('realName');
        let role =await this.session("role");
        self.assign('role',role);
        self.assign('realName',realName);
    }

    //帮助文档
    async helpAction(self){
        let realName =  await this.session('realName');
        let role =await this.session("role");
        self.assign('role',role);
        self.assign('realName',realName);
    }

    //发布设置
    async pubsettingsAction(self){
        let realName =  await this.session('realName');
        let role =await this.session("role");
        self.assign('role',role);
        self.assign('realName',realName);
    }

    //个人中心
    async userinfoAction(self){
        let realName =  await this.session('realName');
        let role =await this.session("role");
        self.assign('role',role);
        self.assign('realName',realName);
    }

    //用户管理
    async usermanagementAction(self){

        let realName =  await this.session('realName');
        let role =await this.session("role");
        self.assign('role',role);
        self.assign('realName',realName);

    }
    //项目管理
    async promanagementAction(self){

        let realName =  await this.session('realName');
        let role =await this.session("role");
        self.assign('role',role);
        self.assign('realName',realName);


    }
    //授权管理
    async rbac1Action(self){

        let realName =  await this.session('realName');
        let role =await this.session("role");
        self.assign('role',role);
        self.assign('realName',realName);

    }

    //授权管理
    async rbac2Action(self){

        let realName =  await this.session('realName');
        let role =await this.session("role");
        self.assign('role',role);
        self.assign('realName',realName);

    }
}