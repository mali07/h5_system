'use strict';

import restBase from './rest-base';

/**
 * rest controller
 * @type {Class}
 */
export default class extends restBase {
  /**
   * init
   * @param  {Object} http []
   * @return {}      []
   */
  init(http){
    super.init(http);
  }
  /**
   * before magic method
   * @return {Promise} []
   */
  async __before() {
  }

  async getAction() {

    let data = this.get();
    data['ownerid'] = await this.session('userId');
    var cates;

    if (!data.cateId) {
      delete data.cateId
    }
    cates = await this.modelInstance.where(data).select();
    return this.success(cates);

  }

  async postAction() {

    let data = this.post();
    data['ownerid'] = await this.session('userId');
    await this.modelInstance.add(data);
    return this.success(data);

  }

    async deleteAction(self){

        let mediaid = self.get().mediaid;

        try{
            let affectid = await this.modelInstance.where({id: ['=', mediaid]}).delete();
            return this.success(affectid);
        }catch (e){
            return this.fail();
        }

    }
}
