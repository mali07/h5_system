'use strict';

import Base from './base.js';

export default class extends Base {
  /**
   * index action
   * @return {Promise} []
   */

  async indexAction(){


      var userId= await this.session("userId");
      if(!userId) return this.fail('Permission ERROR');

      try{

          //判断是否有权限操作该项目
          let projectid = this.get('id');
          let plist = this.model('plist');
          let users = this.model('users');
          let grantor = this.model('grantor');
          let userin =await plist.where({id:projectid,ownerid:userId}).select();
          if(userin.length==0)  {

              let user = await users.where({id:userId}).select();
              let username = user[0].oaname;
              console.log("编辑操作人 : " +username);

              if(user[0].role=='2'){
                  let useringrantor = await grantor.where({pid:projectid,grantorname:username}).select();
                  if(useringrantor.length==0) {
                      return this.fail('Permission ERROR')
                  }

              }
          }

          await this.session('projectid',projectid);
          return this.display();

      }catch(e){
          console.log(e);
          return this.fail();
      }



  }

}