'use strict';

import Base from './base.js';
var sso = require('@58/sso/src/think');
var moment = require('moment');

export default class extends Base {
  /**
   * index action  默认为创意模板页
   * @return {Promise} []
   */

  //默认为创意模板
  async indexAction(self){
      var page = self.http.query['page'] || 1;
      var per = self.http.query['per'] || 8;
      let plist = this.model('plist');
      let count = await plist.where({ispublib: {'=': 1 }}).count();
      let data= await plist.limit(per * (page - 1), per).where({ispublib: {'=': 1 }}).order('id DESC').select();
      self.assign('plist',data);
      self.assign('pagetotal',count);
      self.assign('currentpage',page);
      return self.display();
  }

  async catestAction(){
        var id=  await this.session('testid');
   }


   /**
   *  我的H5，加载该用户下的H5项目
   * */

  async listAction(self) {

    var userId= await this.session("userId");
    var page = self.http.query['page'] || 1;
    var per = self.http.query['per'] || 7;
    var projects = this.model('plist');
    var count = await projects.where({ownerId: {'=': userId }}).count();
    var projectsFound = await projects.limit(per * (page - 1), per).where({ownerId: {'=': userId }}).order('id DESC').select();

    self.assign('projects', projectsFound);
    self.assign('pagetotal',count);
    self.assign('currentpage',page);
    return self.display();
  }

  //用户作品
  async innovateAction(self){
      var page = self.http.query['page'] || 1;
      var per = self.http.query['per'] || 8;
      let plist = this.model('plist');
      let count = await plist.where({isprivate: {'=': 1 }}).count();
      let data= await plist.join([
          'Right JOIN users ON users.id=plist.ownerid'
      ]).where({isprivate: {'=': 1 }}).field('plist.id as id,plist.pagetype,plist.coverurl,plist.pname,plist.createtime,users.name as name').limit(per * (page - 1), per).order('id DESC').select();
      self.assign('plist',data);
      self.assign('pagetotal',count);
      self.assign('currentpage',page);
      return this.display();
  }

    //获取所有用户作品
    async getallcampaignsAction(self){
        let plist = this.model('plist');
        let count = await plist.count();
        let data= await plist.join([
            'Right JOIN users ON users.id=plist.ownerid'
        ]).field('plist.id as id,plist.pagetype,plist.coverurl,plist.pname,plist.createtime,users.name as name').select();
        return this.success({count:count,data:data});
    }



  //帮助文档
  async helpAction(self){
      return this.display();
  }

  //发布设置页面
  async pubsettingsAction(self) {


      var userId= await this.session("userId");
      if(!userId) return this.fail('Permission ERROR');

      try {

          let plist = this.model('plist');
          let projectid=this.get("id");

          let users = this.model('users');
          let grantor = this.model('grantor');
          let userin =await plist.where({id:projectid,ownerid:userId}).select();
          if(userin.length==0)  {

              let user = await users.where({id:userId}).select();
              let username = user[0].oaname;
              console.log("发布设置操作人 : " + username);
              if(user[0].role=='2'){
                  let useringrantor = await grantor.where({pid:projectid,grantorname:username}).select();
                  if(useringrantor.length==0) {
                      return this.fail('Permission ERROR')
                  }
              }
          }

          await this.session('projectid',projectid);
          let prodata = await plist.where({id: {'=': projectid}}).select();
          self.assign('prodata', prodata[0]);

      } catch (e) {
          console.log(e);
          return this.fail();
      }
      return this.display();
  }

  //提交发布设置

  async submitsetAction(self){

      let projectid=await this.session('projectid');
      let param = self.post();
      let plist = this.model('plist');

      try{
          plist.where({id: {'=': projectid}}).update(param);
          return this.success();
      }catch(e){
          console.log(e);
          return this.fail();
      }
  }

  //个人中心
  async userinfoAction(self){
      return this.display();
  }


  //用户管理
    async usermanagementAction(self){


        var page = self.http.query['page'] || 1;
        var per = self.http.query['per'] || 10;

        var userId= await this.session("userId");
        if(!userId) return this.fail('Permission ERROR');

        let users = this.model('users');
        try{
            let count = await users.count();
            let userlist = await users.limit(per * (page - 1), per).select();
            self.assign('userlist',userlist);
            self.assign('pagetotal',count);
            self.assign('currentpage',page);
        }catch(e){
            console.log(e);
        }
        return this.display();
    }

  //项目管理
    async promanagementAction(self){

        var page = self.http.query['page'] || 1;
        var per = self.http.query['per'] || 10;
        var userId= await this.session("userId");
        if(!userId) return this.fail('Permission ERROR');

        let plist = this.model('plist');


        try{

            let count = await plist.where().count();

            let plistdata= await plist.limit(per * (page - 1), per).join([
                'Right JOIN users ON users.id=plist.ownerid'
            ]).field('plist.id as id,plist.pagetype,plist.coverurl,plist.pname,plist.createtime,users.name as name').where({'pname':{'!=':''}}).order('id DESC').select();

            self.assign('plistdata',plistdata);
            self.assign('pagetotal',count);
            self.assign('currentpage',page);

        }catch(e){
            console.log(e);
        }

        return this.display();
    }

  //获取项目授权人列表

    async getgrantorAction(self) {

        var userId = await this.session("userId");
        if (!userId) return this.fail('Permission ERROR');

        var param = this.post();
        var pid = param.pid;

        var grantor = this.model('grantor');

        try {

            //待定，三表查询
            var gdata = await grantor.where({pid: pid}).select();
            return this.success(gdata);
        } catch (e) {
            console.log(e);
        }
        return this.fail();
    }

    //新建授权

    async addnewgrantorAction(){

        var userId = await this.session("userId");
        if (!userId) return this.fail('Permission ERROR');
        var param = this.post();
        var pid = param.pid;  //项目id
        var gname=param.gname;   //授权人姓名

        var plist = this.model('plist');
        var users = this.model('users');
        var grantor = this.model('grantor');

        try{

            var granted=await grantor.where({pid:pid,grantorname:gname}).select();

            if(granted.length!=0){
                return this.success({message:'此用户已授权'});
            }

            var plistinfo=await plist.join([
                'Right JOIN users ON users.id=plist.ownerid'
            ]).field('plist.id as id,users.oaname as name').where({'plist.id':{'=':pid}}).select();

            var gparam = {};
            gparam['pid']=pid;
            gparam['ownername']=plistinfo[0].name;
            gparam['grantorname']=gname;
            gparam['updatetime']=moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

            await grantor.add(gparam);

            return this.success();
        }catch(e){
            console.log(e)
        }
        return this.fail();
    }





    //删除授权
    async deleterbacAction(self){

        var userId= await this.session("userId");
        if(!userId) return this.fail('Permission ERROR');
        var param = this.post();
        var gid=param.gid;
        var grantor = this.model('grantor');
        try {
            await grantor.where({id:gid}).delete();
            return this.success();
        } catch (e) {
            console.log(e);
        }
        return this.fail();
    }


  //授权管理-我的项目

    async rbac1Action(self){

        var userId= await this.session("userId");
        if(!userId) return this.fail('Permission ERROR');

        var page = self.http.query['page'] || 1;
        var per = self.http.query['per'] || 10;

        try{
            var plist = this.model('plist');
            var count = await plist.where({ownerId: {'=': userId }}).count();
            var projectsFound = await plist.limit(per * (page - 1), per).where({ownerId: {'=': userId }}).order('id DESC').select();

            self.assign('projects', projectsFound);
            self.assign('pagetotal',count);
            self.assign('currentpage',page);

            return this.display();
        }catch(e){
          console.log(e)
        }

        return this.fail();

    }

  //授权管理-授权给我的项目
    async rbac2Action(self){

        var page = self.http.query['page'] || 1;
        var per = self.http.query['per'] || 10;

        var userId= await this.session("userId");
        if(!userId) return this.fail('Permission ERROR');

        try{

            var plist = this.model('plist');
            var users=this.model('users');
            var grantor= this.model('grantor');
            var users=await users.where({id:userId}).select();
            var username=users[0].oaname;
            var pdata=await grantor.join([
                'Right JOIN plist ON grantor.pid=plist.id'
            ]).field('plist.id as id,plist.pname,grantor.updatetime,grantor.grantorname').where({'grantorname':username}).select();

            var count=await grantor.where({'grantorname':username}).count();

            self.assign('projects', pdata);
            self.assign('pagetotal',count);
            self.assign('currentpage',page);

            return this.display();

        }catch(e){
            console.log(e)
        }

        return this.fail();
    }

  //退出系统
  async quitAction(self){

      //清cookie,sso登出
      this.cookie('sso-ticket',null);
      var logouturl=think.config("sso").endpoint;
      var service =encodeURIComponent("http://"+self.http.host);
      var url = logouturl+"/logout?service="+service;

      return this.redirect(url);
  }

}