'use strict';

// import uuid from 'node-uuid';
import restBase from './rest-base';
var moment = require('moment');

/**
 * rest controller
 * @type {Class}
 */
export default class extends restBase {
  /**
   * init
   * @param  {Object} http []
   * @return {}      []
   */
  init(http){
    super.init(http);
  }
  /**
   * before magic method
   * @return {Promise} []
   */
  async __before() {
  }

  async getAction(){
      let realName =  await this.session('realName');
      return this.success({realName: realName});
  }

  async postAction(){
      let  userId= await this.session("userId");
      if(!userId) return this.fail('Permission ERROR');

      let param=this.post();
      let uid=param.uid;
      let urole=param.urole;
      try{
          let user = this.model('users');
          let effectuid= await  user.where({id:uid}).update({role:urole});
      }catch(e){
          console.log(e);
      }
      return this.success();
  }

}