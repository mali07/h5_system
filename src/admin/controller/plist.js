'use strict';

// import uuid from 'node-uuid';
import restBase from './rest-base';
var moment = require('moment');

/**
 * rest controller
 * @type {Class}
 */
export default class extends restBase {
  /**
   * init
   * @param  {Object} http []
   * @return {}      []
   */
  init(http){
    super.init(http);
  }
  /**
   * before magic method
   * @return {Promise} []
   */
  async __before() {
  }
}