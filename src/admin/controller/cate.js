'use strict';

import restBase from './rest-base';

/**
 * rest controller
 * @type {Class}
 */
export default class extends restBase {
  /**
   * init
   * @param  {Object} http []
   * @return {}      []
   */
  init(http){
    super.init(http);
  }
  /**
   * before magic method
   * @return {Promise} []
   */

  async __before() {
  }

  async getAction() {
      let userid = await this.session('userId');
      let cates = await this.modelInstance.where({ownerid: {'=': userid}}).select();
      return this.success(cates);
  }

  async postAction() {
    let data = this.post();
    data['ownerid'] = await this.session('userId');
    let insertId = await this.modelInstance.add(data);
    data['id'] = insertId;
    return this.success(data);
  }

  async deleteAction(self){

    let cateid = self.get().cateid;

    try{
        let affectid = await this.modelInstance.where({id: ['=', cateid]}).delete();
        return this.success(affectid);
    }catch (e){
        return this.fail();
    }

  }

}