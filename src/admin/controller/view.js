'use strict';

import Base from './base.js';

export default class extends Base {
  /**
   * index action
   * @return {Promise} []
   */
  async indexAction(self){

    var tpltype=self.get("tpltype");

    var id = this.get('id');
    var linknum = parseInt(self.get("linknum"));

    if (!id) {
      return this.fail(400)
    }

    var project = this.model('project');

    console.log(id);

    var projectFound = await project.where({
      id: id
    }).find();



    if (!think.isEmpty(projectFound)) {

      //长页面，按页面id进行分页处理
      if(tpltype==2){

          var rawdata=JSON.parse(projectFound.rawdata).data;
          this.assign('data',rawdata.pages[linknum-1]);
          this.assign('trackUrl',rawdata.trackUrl);
          this.assign('shadedata',rawdata.pages);
          this.assign('bgdata',rawdata.back);

          return this.display(think.ROOT_PATH+"/view/admin/view_long.html");
      }else{
          this.assign('data', JSON.parse(projectFound.rawdata).data);
          return this.display();
      }
    } else {
      return this.fail(404);
    }
  }
}