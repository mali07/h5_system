'use strict';

// import uuid from 'node-uuid';
import restBase from './rest-base';
var moment = require('moment');

/**
 * rest controller
 * @type {Class}
 */
export default class extends restBase {
  /**
   * init
   * @param  {Object} http []
   * @return {}      []
   */
  init(http){
    super.init(http);
  }
  /**
   * before magic method
   * @return {Promise} []
   */
  async __before(){
  }

  async postAction() {


      let data = this.post();
      let sorpid = Number(data.pid);
      delete data.pid;
      let ownerId = await this.session("userId");
      data['ownerid'] = ownerId;
      data['createtime'] = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

      if(think.isEmpty(data)){
          return this.fail("data is empty");
      }

      //查看项目名是否已经存在，并给出相应提示

      let plist= this.model('plist');
      let project = this.model('project');
      let isExist = await plist.where({pname: {'=': data.pname }}).select();
      if(isExist.length > 0){
          return this.fail("项目名已存在，请重新提交");
      }else{
          try{
              var proid = await plist.add(data);

              if(sorpid==""||sorpid==undefined){
                  await project.add({id:proid,rawdata:"",update:data['createtime']});
              }else{
                  let sorplist = await plist.where({id: sorpid }).select();
                  let sorproject = await project.where({id:sorpid}).select();
                  await plist.where({id: {'=': proid}}).update({"pagetype":sorplist[0].pagetype});
                  await project.add({id:proid,rawdata:sorproject[0].rawdata,update:data['create']});
              }

              await this.session('projectid',proid);
              return this.success({id: proid});

          }catch(e){
              console.log(e);
              return this.fail();
          }
      }

    };

    async deleteAction(){
        if (!this.id) {
            return this.fail('params error');
        }
        let pid=this.id;
        let plist = this.model("plist");
        let rows = plist.where({id : pid}).delete();
        return this.success({affectedRows: rows});
    };

}