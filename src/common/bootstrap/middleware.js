/**
 * this file will be loaded before server started
 * you can register middleware
 * https://thinkjs.org/doc/middleware.html
 */

/**
 *
 * think.middleware('xxx', http => {
 *   
 * })
 *
 */


var sso = require('@58/sso/src/think');

var authConfig = {

    baseUrl: think.config('sso').endpoint,
    cookieName:think.config('cookie').name,
    sso_servers:think.config('server'),
    bspKey:think.config('bsp').appkey,
    bspSecret:think.config('bsp').appsecret,
    bspHost: think.config('bsp').bspHost,
};

var sessionAdapter = {
    _sso_get: think.cache,
    _sso_set: think.cache,
    _sso_del: function (key) {
        return think.cache(key, null);
    }};

think.middleware('sso', sso(authConfig, sessionAdapter));