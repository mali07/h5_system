/**
 * this file will be loaded before server started
 * you can define global functions used in controllers, models, templates
 */

/**
 * use global.xxx to define global functions
 * 
 * global.fn1 = function(){
 *     
 * }
 */

import CAS from '@chr/cas';

global.sso = new CAS({
    'base_url': 'https://sso.test.58.com:8443/gsso',
    'service': 'http://localhost:8361/admin/index',
    'version': '2.0',
    'ignoreSslError': true
});