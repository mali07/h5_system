'use strict';

/**
 * session configs
 */
export default {
  name: 'op_session',
  type: 'redis',
  // secret: '*3C%USO~',
  // secret: '',
  timeout: 24 * 3600,
  cookie: { // cookie options
    length: 32,
    httponly: true
  }
};