/**
 * Created by chiqingzhen on 02/12/2016.
 */


export default [
    ['admin/edit/:id', 'admin/edit/index'],
    ['admin/view/:id', 'admin/view/index']
]