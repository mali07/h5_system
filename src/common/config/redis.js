/**
 * Created by chiqingzhen on 29/11/2016.
 */


export default {
    host: think.config('redis').host,
    port: think.config('redis').port,
    password: '',
    timeout: 0,
    log_connect: true
};