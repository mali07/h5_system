'use strict';

export default {
    cookie:{
        name:'sso-ticket'
    },

    server:['192.168.177.150'],

    sso: {
        endpoint: 'https://sso.test.58.com:8443/gsso'
    },
    bsp: {
        appkey: 'chrop',
        appsecret: '96a7956befe68ad45172baae7fcf5148',
        bspHost:'http://t.union.web.58dns.org:11001'
    },
    db: {
        mysql: {
            host: '127.0.0.1',
            port: '3306',
            database: 'chr_op',
            user: 'root',
            password: 'chinahr-fe',
            prefix: '',
            encoding: 'utf8'
        }
    },
    redis:{
        host: '127.0.0.1',
        port: 6379,
    }
};