'use strict';
/**
 * template config
 */

import querystring from 'querystring';
import _ from 'lodash';


export default {
  type: 'nunjucks',
  content_type: 'text/html',
  file_ext: '.html',
  file_depr: '_',
  root_path: think.ROOT_PATH + '/view',
  adapter: {
    nunjucks: {
      prerender: function (nunjucks, env) {
        env.addFilter('pic', function (name) {
          return 'http://pic1.58cdn.com.cn' + name
        });
        env.addFilter('style', function (styleObj) {
          var newStyle = {};
          Object.keys(styleObj).forEach(key => {
            if ((styleObj[key]).toString().trim() != '') {
              newStyle[_.kebabCase(key)] = styleObj[key];
            }
          });
          return querystring.stringify(newStyle, ';', ':', {
            encodeURIComponent: t => t
          })
        })
      }
    }
  }
};