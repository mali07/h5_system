
Application created by [ThinkJS](http://www.thinkjs.org)

## Install dependencies

```
npm install
```

## Start server

```
npm start
```

## Deploy with pm2

Use pm2 to deploy app on production enviroment.

```
pm2 startOrReload pm2.json
```

## 配置

需要 MySQL 和 redis，具体见配置文件

需要复制 src/common/config/evn/development.smaple.js 到 development.js