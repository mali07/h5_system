/**
 * Created by gaoying on 2017/6/22.
 */

var entity = {

    init:function(){
       var _this = this;
       this.initSwiper();
       this.listen();
       this.showqrcode();
       this.audioAutoPlay('media');
       setTimeout(function(){
           _this.renderData();
       },1000)
    },

    initSwiper:function(){
        var that = this;
        window.mySwiper = new Swiper('.swiper-container', {
            effect : effect,
            direction:dir,
            loop: false,
            speed: timer,
            noSwiping : true,
            onInit:function (swiper) {

                $("#page1 .componentids").each(function(){
                    var animateattr=$(this).html()
                    var aid=animateattr.split("#")[0];
                    var atype=animateattr.split("#")[1];
                    $("."+aid+"").each(function(){
                        $(this).show();
                        that.animateCss($(this),atype);
                    });

                })
            },
            onSlideChangeStart: function(swiper){
                var activeIndex=swiper.activeIndex+1;
                $("#page"+activeIndex+" .componentids").each(function(){
                    var animateattr=$(this).html()
                    var aid=animateattr.split("#")[0];
                    $("."+aid+"").each(function(){
                        $(this).hide()
                    })
                })
            },
            onSlideChangeEnd: function(swiper){
                var activeIndex=swiper.activeIndex+1;
                $("#page"+activeIndex+" .componentids").each(function(){
                    var animateattr=$(this).html()
                    var aid=animateattr.split("#")[0];
                    var atype=animateattr.split("#")[1];
                    $("."+aid+"").each(function(){
                        $(this).show();
                        that.animateCss($(this),atype);
                    })

                })
            }
        });
    },
    rd:function(m,n){
        var c = m-n+1;
        return Math.floor(Math.random() * c + n);
    },
    gopage:function(btntype,linknum){

        if(btntype!=''){

            switch(btntype){

                case 'linkUrl':
                    window.location.href=linknum;
                    break;
                case 'linkPage':
                    window.mySwiper.slideTo(linknum-1,0);
                    break;
                case 'popPage':
                    $("#shade-"+linknum).toggle();
                    break;
                case 'randomPage':
                    var total=$(".shade").length;
                    var rdnumber = this.rd(0,total-1);
                    $(".shade").eq(rdnumber).toggle();
                    break;
                case 'backToTop':
                    window.mySwiper.slideTo(0,0);
                    break;
            }
        }

    },

    animateCss:function(element,animationName){
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        element.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    },

    audioAutoPlay:function(e){
        var n = document.getElementById(e);
        i = function () {
            n.play(),
            document.removeEventListener("touchstart", i, !1)
        };
        document.addEventListener("WeixinJSBridgeReady", function () {
            i()
        }, !1),

        document.addEventListener("touchstart", i, !1)
    },

    showqrcode:function(){

        //显示二维码
        var linkurl ="http://chrpage.chinahr.com/home/view/"+window.location.href.split("/")[5];
        $("#linkurl").attr("href",linkurl);
        $("#linkurl").html(linkurl);
        $("#qrcode").qrcode({
            text: linkurl
        });

    },

    listen:function(){

        $(".shade").click(function(){
            $(this).toggle();
        });

        $("#audio").click(function(e){
            $(this).toggleClass('play');
            $(this).hasClass('play')?$('audio')[0].play():$('audio')[0].pause();
        });

        //表单提交
        $("form").submit(function () {
            var method=$("#submit").attr("data-reqmethod");
            var url=$("#submit").attr("data-requrl");
            var data=$("#form").serialize();
            data=decodeURIComponent(data,true);
            $.ajax({
                type: method,
                url:url,
                data:data,
                success: function (data) {
                    if(data.message==""){
                        alert("提交成功");
                    }else{
                        alert("提交失败");
                    }

                }
            });
            return false;
        });
    },


    renderData:function(){
        var _this = this;
        var dataContainer = $("#dataContainer");
        var datasource = $("#dataContainer").attr("data-source");
        var dataItem = $("#dataItem");
        var dataDetail = $("#dataDetailInfo");
        var _bgcolor = dataItem.css('background-color') || "#fff";
        var _color = dataItem.css('color') || "#000";
        var nextIcon = $("#dataItem #nextIcon").html() || '';
        var cardType = $("#dataItem #cardType").html();

        if(cardType == "ct01"){

            $.ajax({
                type:'get',
                url:datasource,
                dataType: "jsonp",
                jsonp: "callback",
                success: function (data) {
                    if(data.entity){

                        var _data = data.entity;
                        var _str = "<ul class='dataItemContainer' style='width:"+dataItem.css('width')+";margin: auto;padding-top:15px;padding-bottom:15px;'>"

                        for(var item in _data){
                            _str+="<a href="+_data[item].link+"><li style='width:"+dataItem.css('width')+";height:"+dataItem.css('height')+";line-height:"+dataItem.css('height')+";background-color:"+_bgcolor+";margin-bottom:8px;border-radius:"+dataItem.css('border-radius')+";color:"+dataItem.css('color')+"'><span>"+_data[item].name+"</span><span><img src='"+nextIcon+"' alt=''></span></li></a>"
                        }
                        _str += "</ul>"
                        dataContainer.append(_str);
                    }
                }
            });

        }else{
            //点击展开详情
            $.ajax({
                type:'get',
                url:datasource,
                dataType: "jsonp",
                jsonp: "callback",
                success: function (data) {
                    if(data.entity){

                        var _data = data.entity;
                        var _str = "<ul class='dataItemContainer' style='width:"+dataItem.css('width')+";margin: auto;padding-top:15px;padding-bottom:15px;'>"

                        for(var item in _data){
                            _str+="<li ><div class='title' style='width:"+dataItem.css('width')+";height:"+dataItem.css('height')+";line-height:"+dataItem.css('height')+";background-color:"+_bgcolor+";margin-bottom:8px;border-radius:"+dataItem.css('border-radius')+";color:"+dataItem.css('color')+"'><span>"+_data[item].name+"</span><span style='font-size:12px;' onClick='showDetail()' touchend='showDetail()'>详情<img  style='margin-left:6px;position:relative;top:0px;' src='"+nextIcon+"' alt=''></span></div><div style='display:none;width:"+dataDetail.css('width')+";height:"+dataDetail.css('height')+";background-color: "+dataDetail.css('background-color')+";color:"+dataDetail.css('color')+"' class='detail'><div class='content'>"+_data[item].detail+"</div></div></li>"
                        }
                        _str += "</ul>"
                        dataContainer.append(_str);
                    }
                }
            });

        }
    },


};

function showDetail(){
   $(event.target.parentElement.nextElementSibling).toggle();
}

entity.init();
