/**
 * Created by gaoying on 2017/6/22.
 */

var entity = {



    init:function(){

        this.listen();
        this.initSwiper();
    },

    initSwiper(){

        $(".swiper-container").each(function(){

            var eleId=$(this).attr("id");
            var groupNum = parseInt($("#groupNum_"+eleId).html());
            var groupSpace = parseInt($("#groupSpace_"+eleId).html());
            var spaceTime = parseInt($("#spaceTime_"+eleId).html());
            var auto=$("#auto_"+eleId).html();
            new Swiper('#'+eleId+'', {
                autoplay: spaceTime,
                loop:auto,
                slidesPerView:groupNum,
                spaceBetween:groupSpace
            });
        });
    },

    rd:function(n,m){
        var c = m-n+1;
        return Math.floor(Math.random() * c + n);
    },

    gopage:function(btntype,linknum){

        if(btntype!=''){

            switch(btntype){

                case 'none':break;
                case 'linkUrl':
                    window.location.href=linknum;
                    break;
                case 'linkPage':
                    window.location.href=window.location.href.split("&")[0]+"&linknum="+linknum;
                    break;
                case 'popPage':
                    $("#shade-"+linknum).css({'display':'block'});
                    break;
                case 'randomPage':
                    var total=$(".shade").length;
                    var rdnumber =this.rd(0,total-1);
                    $(".shade").eq(rdnumber).css({'display':'block'});
                    break;
                case 'backToTop':
                    $('body,html').animate({ scrollTop: 0 }, 200);
                    break;
                case 'anchor' :
                    window.location.href='#'+linknum;
                    break;

            }
        }
    },
    listen:function(){
        //表单提交
        $("form").submit(function () {
            var method=$("#submit").attr("data-reqmethod");
            var url=$("#submit").attr("data-requrl");
            var data=$("#form").serialize();
            data=decodeURIComponent(data,true);
            $.ajax({
                type: method,
                url:url,
                data:data,
                success: function (data) {
                    if(data.message==""){
                        alert("提交成功");
                    }else{
                        alert("提交失败");
                    }

                }
            });
            return false;
        });

        $(".shade").click(function(){
            $(this).css({'display':'none'})
        });

    }
};

entity.init();
