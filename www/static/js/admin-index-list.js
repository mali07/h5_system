/**
 * Created by chiqingzhen on 02/12/2016.
 */


var listPage = {

    param:{
        pid:""
    },

    init: function () {
        $('.item.new').click(listPage.newH5);
        $('.templates').delegate('.cover', 'click', listPage.redirectToEdit);
        $('.action.edit').click(listPage.edit);
        $('.action.delete').click(listPage.delete);
        $('.action.setting').click(listPage.setting);
        $('.action.preview').click(listPage.preview);


    },
    newH5: function () {
        $("#shadow").show();
        $("#dl_newh5").show();
        $("#submit").click(listPage.submit);
        $("#cancel").click(listPage.cancel);
    },

    submit:function(){

        var name=$("#name").val();
        if (!name) return;
        var pagetype=$('#dl_newh5 input:radio[name="pagetype"]:checked').val();

        $.ajax({
            url: '/admin/project',
            method: 'POST',
            dataType: 'json',
            data: {
                pname: name,
                pagetype:pagetype,
                pid:listPage.param.pid,
            },
            success: function (data) {

                if(data.errmsg!=""){
                    alert(data.errmsg)
                }else{
                    $("#shadow").hide();
                    $("#dl_newh5").hide();
                    window.location.href = '/admin/edit/' + data.data.id
                }

            },
            error: function (e) {
                alert('新建项目发生错误');

            }
        });


    },
    cancel:function(){
        $("#shadow").hide();
        $("#dl_newh5").hide();
    },

    redirectToEdit: function () {
        var id = $(this).data('projectid');
        if (!id) return;
        location.href = '/admin/edit/' + id;
    },
    delete: function (event) {
        var id = $(this).parent().data('projectid');
        var ensure = confirm('确定删除吗？');
        if(ensure) {
            $.ajax({
                method: "DELETE",
                url: '/admin/project/' + id,
                success: function (data) {
                    $("[data-projectid="+id+"]").fadeOut(300);
                },
                error: function (err) {
                    console.log(err);
                }
            })
        }
        event.stopPropagation();
    },
    //发布设置
    setting:function(){
        var id = $(this).parent().data('projectid');
        if (!id) return;
        location.href = '/admin/index/pubsettings?id=' + id;
    },
    //预览页面
    preview:function(){
        var id = $(this).parent().data('projectid');
        var pagetype=$("#pagetype"+id).html();
        if (!id) return;
        window.open('/admin/view/' + id + '?tpltype='+pagetype+"&linknum=1");
    },
    //编辑按钮
    edit:function(){
        var id = $(this).parent().data('projectid');
        if (!id) return;
        location.href = '/admin/edit/' + id;
    }
};

listPage.init();