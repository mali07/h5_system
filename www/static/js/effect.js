/**
 * Created by gaoying on 2017/3/3.
 */


var effect = {

    management:{
       uid:"",
       pid:"",
       urole:"",
       gid:"",
       gname:""
    },

    formEntity: {
        pname: '',
        ptitle: '',
        isprivate: Number($("#privatevalue").html()),
        ispublib: Number($("#pubvalue").html()),
        coverurl: '',
        stitle: '',
        scontent: '',
        simgurl: '',
        pubnow:Number($("#ispub").html())

    },

    init: function () {
        this.initState();
        this.listen();
        $('#pagelist').click(effect.pagination);
        $('#prepage').click(effect.prepage);
        $('#nextpage').click(effect.nextpage);
    },

    pagination:function(e){
        var pageindex = $(e.target).html();
        window.location=window.location.href.split("?")[0]+"?page="+pageindex;
    },

    prepage:function(){
        var pageindex = parseInt($("#currentid").html());
        if(pageindex > 1){
            pageindex-=1;
            window.location=window.location.href.split("?")[0]+"?page="+pageindex;
        }
    },

    nextpage:function(){
        debugger
        var pageindex = parseInt($("#currentid").html());
        var totalpage = parseInt($("#totalCount").html());
        if(pageindex < totalpage){
            pageindex+=1;
            window.location=window.location.href.split("?")[0]+"?page="+pageindex;
        }
    },

    initState: function () {

        var currentid  = $("#currentid").html();
        $($("#pagelist li")[currentid-1]).addClass('activepage');

        if(this.formEntity.isprivate ==true){
            $("#isprivate").css("background-image", "url('/static/img/yes_icon.png')")
        }else{
            $("#isprivate").css("background-image", "url('/static/img/no_icon.png')")
        }

        if(this.formEntity.ispublib==true){
            $("#ispublib").css("background-image", "url('/static/img/yes_icon.png')")
        }else{
            $("#ispublib").css("background-image", "url('/static/img/no_icon.png')")
        }

        if(this.formEntity.pubnow ==1){
            $("#ispub").html("已发布");
        }else{
            $("#ispub").html("未发布");
        }


    },

    getForms: function () {

        this.formEntity.pname = $("#pname").val();
        this.formEntity.ptitle = $("#ptitle").val();
        this.formEntity.coverurl = $("#coverurl").val();
        this.formEntity.stitle = $("#stitle").val();
        this.formEntity.scontent = $("#scontent").val();
        this.formEntity.simgurl = $("#simgurl").val();
    },

    submitConfig:function(){
        var that = this;
        that.getForms();
        $.ajax({
            url: '/admin/index/submitset',
            method: 'POST',
            dataType: 'json',
            data: that.formEntity,
            success: function (data) {
                if (data.errmsg == "") {
                    alert("设置成功");
                    location.href = "/admin/index/list";
                } else {
                    alert(data.errmsg);
                }
            },
            error: function (err) {
            }
        })
    },

    usetpl:function(pid){

        $.ajax({
            url: '/admin/index/usetpl',
            method: 'POST',
            dataType: 'json',
            data: {pid:pid},
            success: function (data) {

            },
            error: function (err) {
            }
        })

    },

    resetUserRole:function(){
        var that = this;
        $.ajax({
            url: '/admin/users/update',
            method: 'POST',
            dataType: 'json',
            data:that.management,
            success: function (data) {
               if(!data.errmsg){
                   alert('授权成功');
               }else{
                   alert('授权失败，请稍后再试 ！');
               }
               $('.confirm').hide();
               location.reload();
            },
            error: function (err) {
            }
        })
    },


    renderGrantors:function(data){
        $("#grantorlist").empty();
        var str="";
        for(var i=0;i<data.length;i++){
            str+='<tr><td>'+(i+1)+'</td><td>'+data[i].ownername+'</td><td>'+data[i].grantorname+'</td><td>'+data[i].updatetime+'</td><td><span style="color:red;" data-gid="'+data[i].id+'" class="deleterbac">删除授权</span></td></tr >'
        }
        $("#grantorlist").append(str);
        $('.confirm').show();
        this.initDetelerbac();
    },

    getGrantors:function(){
        var that = this;
        $.ajax({
            url: '/admin/index/getgrantor',
            method: 'POST',
            dataType: 'json',
            data:that.management,
            success: function (data) {
                if(!data.errmsg){
                   that.renderGrantors(data.data);
                }else{
                    alert('请求失败，请稍后再试!');
                }
            },
            error: function (err) {
            }
        })
    },

    addNewGrantor:function(){
        var that = this;
        $.ajax({
            url: '/admin/index/addnewgrantor',
            method: 'POST',
            dataType: 'json',
            data:that.management,
            success: function (data) {
                if(!data.errmsg){
                    if(data.data.message){
                       alert(data.data.message);
                    }else{
                       that.getGrantors();
                    }
                }else{
                    alert('请求失败，请稍后再试!');
                }
            },
            error: function (err) {
            }
        })

    },


    initDetelerbac:function(){

        var that=this;

        $(".deleterbac").click(function(e){

            that.management.gid=$(e.target).attr('data-gid');;
            var ensure = confirm('确定删除吗？');
            if(ensure) {
                $.ajax({
                    method: "POST",
                    url: '/admin/index/deleterbac',
                    dataType:'json',
                    data:that.management,
                    success: function (data) {
                        debugger;
                        if (data.errmsg == "") {
                           that.getGrantors();
                        }else{
                           alert('删除失败，请稍后再试');
                        }
                    },
                    error: function (err) {
                        console.log(err);
                    }
                })
            }
            event.stopPropagation();
        })


    },

    listen: function () {

        var that = this;

        $("#profile").click(function () {
            $("#mylist").toggle();
        });

        $("#isprivate").click(function () {

            that.formEntity.isprivate = (-1) * that.formEntity.isprivate;
            if (that.formEntity.isprivate==true) {
                $(this).css("background-image", "url('/static/img/yes_icon.png')");
            } else {
                $(this).css("background-image", "url('/static/img/no_icon.png')");
            }
        });

        $("#ispublib").click(function () {
            that.formEntity.ispublib = (-1) * that.formEntity.ispublib;
            if (that.formEntity.ispublib==true) {
                $(this).css("background-image", "url('/static/img/yes_icon.png')");
            } else {
                $(this).css("background-image", "url('/static/img/no_icon.png')");
            }
        });

        $("#submitset").click(function (e) {
            that.formEntity.pubnow = 1;
            that.submitConfig();
        });

        $("#saveset").click(function (e) {
            that.formEntity.pubnow = 0;
            that.submitConfig();
        });


        $("#cancelset").click(function (e) {
            location.href = "/admin/index/list";
        });

        //item  hover 出现二维码
        $(".item").hover(function(e){

            var pid=e.target.getAttribute("data-projectid");
            var pagetype=$("#pagetype"+pid).html();
            $("#qt"+pid).qrcode({
                text: "http://chrpage.chinahr.com/home/view/"+pid+"?tpltype="+pagetype+"&linknum=1"
            });
            $("#sd"+pid).show();
        },function(){
             $(".shadow").hide();
             $(".qrcode").empty();
        });

        //立即使用模板
        $(".usetpl").click(function(e){
            var pid=e.target.parentElement.getAttribute("data-projectid");
            listPage.param.pid=pid;
            listPage.newH5();
        })

        //用户授权
        $('.userAuthorize').click(function(e){

            var uid = $(e.target).attr('id').split('_')[1];
            that.management.uid=uid;
            $('.confirm').show();
        });

        $("#userSubmit").click(function(e){
            var urole= $("input[type='radio']:checked").val();
            that.management.urole=urole;
            that.resetUserRole();
        });

        $(".cancel").click(function(){
            $('.confirm').hide();
        });

        $(".dialogs").click(function(){
            $(".confirm").hide();
        });


        //项目管理操作
        $('.proview').click(function(e){
            var pid=$(e.target).attr('data-pid');
            var pagetype=$(e.target).attr('data-pagetype');
            if (!pid) return;
            window.open('/admin/view/' + pid + '?tpltype='+pagetype+"&linknum=1");
        });

        $('.proedit').click(function(e){
            var pid=$(e.target).attr('data-pid');
            window.location.href='http://'+window.location.host+'/admin/edit/'+pid;
        });

        $('.prosetting').click(function(e){
            var pid=$(e.target).attr('data-pid');
            window.location.href='http://'+window.location.host+'/admin/index/pubsettings?id='+pid;
        });
        $('.prorbac').click(function(e){
            var id=$(e.target).attr('data-pid');
            that.management.pid=id;
            that.getGrantors();
        });
        $('.prodetele').click(function(e){
            var id=$(e.target).attr('data-pid');
            var ensure = confirm('确定删除吗？');
            if(ensure) {
                $.ajax({
                    method: "DELETE",
                    url: '/admin/project/' + id,
                    success: function (data) {
                        location.reload();
                    },
                    error: function (err) {
                        console.log(err);
                    }
                })
            }
            event.stopPropagation();
        });

        $("#addnewuser").click(function(){
            var gname=$("#uname").val();
            if(!gname) return ;
            that.management.gname=gname;
            that.addNewGrantor();

        })

    }

};


effect.init();
