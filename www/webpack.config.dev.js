/**
 * Created by chiqingzhen on 04/12/2016.
 */

var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: path.resolve(__dirname, '../view/admin/edit/main.js'),
    output: {
        path: path.resolve(__dirname, '../www/static/edit'),
        publicPath: '/static/edit/',
        filename: 'js/edit-bundle.[hash:4].js'
    },
    module: {
        loaders: [
            {
                test: /\.vue$/,
                loader: 'vue'
            },
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/
            },
            {
                test: /\.(jpg|png|svg|eot|woff|ttf|woff2|gif)$/,
                loader: "url",
                query: {
                    limit: 10,
                    name: "images/[name].[hash:6].[ext]"
                }
            }
        ]
    },
    devtool: '#eval-source-map',
    plugins: [
        new HtmlWebpackPlugin({
            filename: path.resolve(__dirname, '../view/admin/edit_index.html'),
            template: path.resolve(__dirname, '../view/admin/edit/edit_template.html')}),
        new ExtractTextPlugin("css/edit.[hash:6].css")
    ],
    vue: {
        loaders: {
            css: ExtractTextPlugin.extract("css"),
            scss: ExtractTextPlugin('css!sass')
        }
    },

}