# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.52)
# Database: chr_op
# Generation Time: 2017-03-09 03:59:54 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cate`;

CREATE TABLE `cate` (
  `id` varchar(40) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `media_type` varchar(45) DEFAULT NULL,
  `ownerId` varchar(45) DEFAULT NULL,
  `groupId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `cate` WRITE;
/*!40000 ALTER TABLE `cate` DISABLE KEYS */;

INSERT INTO `cate` (`id`, `name`, `type`, `media_type`, `ownerId`, `groupId`)
VALUES
	('2b2c6640-f73b-11e6-bffa-c98ee0457b56','news','media','image',NULL,NULL),
	('65e94c00-f73e-11e6-bffa-c98ee0457b56','test','media','image',NULL,NULL),
	('c21abc20-c354-11e6-816f-2dd023506d48','ssss','media','image','e84ed360-b614-11e6-846e-85ad7f6b98b5',NULL),
	('c781a1b0-c1e2-11e6-ba25-69e89a3c490c','新分类','media','image','dd45d7a0-b611-11e6-b8eb-5b7aab60e12d',NULL),
	('e8b2ba30-c1d9-11e6-9fe1-e36eda50b765','新分类','media','image','dd45d7a0-b611-11e6-b8eb-5b7aab60e12d',NULL),
	('ed50fa70-c1d9-11e6-9fe1-e36eda50b765','哈哈哈， 什么也没有','media','image','dd45d7a0-b611-11e6-b8eb-5b7aab60e12d',NULL);

/*!40000 ALTER TABLE `cate` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `id` varchar(40) NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table group_media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_media`;

CREATE TABLE `group_media` (
  `id` varchar(40) NOT NULL,
  `media_id` varchar(45) DEFAULT NULL,
  `group_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table group_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group_user`;

CREATE TABLE `group_user` (
  `id` varchar(40) NOT NULL,
  `group_id` varchar(45) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` varchar(40) NOT NULL,
  `path` varchar(100) DEFAULT NULL,
  `groupId` varchar(45) DEFAULT NULL,
  `cateId` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;

INSERT INTO `media` (`id`, `path`, `groupId`, `cateId`, `type`)
VALUES
	('03751300-c1e3-11e6-ba25-69e89a3c490c','/test/n_v1bl2lwtkyczivqs73yzqa.png',NULL,'','image'),
	('2f925870-f73b-11e6-bffa-c98ee0457b56','/test/n_v1bl2lwkezswvfqnh5lf4a.jpg',NULL,'2b2c6640-f73b-11e6-bffa-c98ee0457b56','image'),
	('32c05780-c1e0-11e6-965c-5169dab16196','/test/n_v1bkuyfvm7cfivqpfgmf7q.png',NULL,'e8b2ba30-c1d9-11e6-9fe1-e36eda50b765','image'),
	('5828e1b0-c1e3-11e6-ba25-69e89a3c490c','/test/n_v1bkujjd7gczivqs263vwq.png',NULL,'','image'),
	('64623960-c1e1-11e6-ba25-69e89a3c490c','/test/n_v1bkujjd5acnivqa4razha.png',NULL,'ed50fa70-c1d9-11e6-9fe1-e36eda50b765','image'),
	('6a396ed0-c1e1-11e6-ba25-69e89a3c490c','/test/n_v1bj3gzsfkcnivrk25erfq.jpg',NULL,'ed50fa70-c1d9-11e6-9fe1-e36eda50b765','image'),
	('72cf0850-c1de-11e6-965c-5169dab16196','/test/n_v1bl2lwkfpbzivq3wnqnuq.png',NULL,'','image'),
	('ce600fd0-c354-11e6-816f-2dd023506d48','/test/n_v1bj3gzsf5qjjvqrkrja3q.gif',NULL,'c21abc20-c354-11e6-816f-2dd023506d48','image'),
	('ea5dbc00-c1e2-11e6-ba25-69e89a3c490c','/test/n_v1bl2lwtjoczivr2u4vqxa.jpg',NULL,'c781a1b0-c1e2-11e6-ba25-69e89a3c490c','image');

/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` varchar(40) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `cover` varchar(45) DEFAULT NULL,
  `categoryId` varchar(45) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `rawData` text NOT NULL,
  `ownerId` varchar(45) DEFAULT NULL,
  `groupId` varchar(45) DEFAULT NULL,
  `pagetype` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;

INSERT INTO `project` (`id`, `name`, `cover`, `categoryId`, `createTime`, `rawData`, `ownerId`, `groupId`, `pagetype`)
VALUES
	('b3e0d660-0479-11e7-b2ec-cd7ac097c1bc','test01',NULL,NULL,'2017-03-09 03:37:21','{\"data\":{\"front\":{\"id\":\"page-1489030641512693\",\"selected\":true,\"type\":\"page\",\"attr\":{},\"style\":{\"backgroundColor\":\"transparent\",\"backgroundImage\":\"\",\"backgroundSize\":\"contain\",\"backgroundPosition\":\"center\",\"backgroundRepeat\":\"no-repeat\",\"opacity\":100},\"components\":[]},\"back\":{\"id\":\"page-1489030641512882\",\"selected\":true,\"type\":\"page\",\"attr\":{},\"style\":{\"backgroundColor\":\"white\",\"backgroundImage\":\"\",\"backgroundSize\":\"contain\",\"backgroundPosition\":\"center\",\"backgroundRepeat\":\"no-repeat\",\"opacity\":100},\"components\":[]},\"pages\":[{\"id\":\"page-1489030641512675\",\"selected\":true,\"type\":\"page\",\"attr\":{\"tplHeight\":\"650px\"},\"style\":{\"backgroundColor\":\"transparent\",\"backgroundImage\":\"\",\"backgroundSize\":\"contain\",\"backgroundPosition\":\"center\",\"backgroundRepeat\":\"no-repeat\",\"opacity\":100},\"components\":[{\"type\":\"image\",\"id\":1489030643887,\"attr\":{\"src\":\"/test/n_v1bl2lwkezswvfqnh5lf4a.jpg\"},\"moved\":false,\"selected\":false,\"style\":{\"position\":\"absolute\",\"top\":\"34px\",\"left\":\"27px\",\"height\":\"240px\",\"width\":\"320px\",\"color\":\"#000\",\"padding\":0,\"backgroundColor\":\"transparent\",\"fontFamily\":\"\",\"fontWeight\":\"\",\"fontStyle\":\"\",\"textDecoration\":\"\",\"textAlign\":\"\",\"lineHeight\":\"\",\"transfrom\":\"\",\"opacity\":\"\",\"borderStyle\":\"solid\",\"borderWidth\":\"0\",\"borderColor\":\"blue\",\"borderRadius\":\"\",\"transform\":\"rotate(undefineddeg)\"}},{\"type\":\"text\",\"id\":1489030648370,\"attr\":{\"content\":\"zshcjgjdgj\"},\"moved\":false,\"selected\":false,\"style\":{\"fontSize\":\"18px\",\"position\":\"absolute\",\"top\":\"305px\",\"left\":\"137px\",\"height\":\"45px\",\"width\":\"100px\",\"color\":\"#000\",\"padding\":0,\"backgroundColor\":\"transparent\",\"backgroundImage\":\"\",\"backgroundSize\":\"contain\",\"backgroundPosition\":\"center\",\"backgroundRepeat\":\"no-repeat\",\"fontFamily\":\"\",\"fontWeight\":\"\",\"fontStyle\":\"\",\"textDecoration\":\"\",\"textAlign\":\"center\",\"lineHeight\":\"40px\",\"transform\":\"rotate(0deg)\",\"transformrotate\":0,\"opacity\":1,\"borderRadius\":\"\",\"whiteSpace\":\"pre-line\"}}]}]},\"status\":{\"tplType\":\"pt02\",\"activePageIndex\":0,\"ComponentsMaxIndex\":0,\"activeComponentsIndex\":0,\"pageType\":\"current\",\"activeComponents\":[],\"clipboard\":[],\"clipPageBoard\":\"\",\"ctxMenu\":{\"type\":\"main\",\"display\":\"none\",\"pointX\":763,\"pointY\":458},\"border\":{\"boardBorder\":\"board-border-none\",\"center\":\"block\",\"middle\":\"none\"}}}',NULL,NULL,'pt02');

/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` varchar(40) NOT NULL,
  `oaname` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `department` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `oaname_UNIQUE` (`oaname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `oaname`, `name`, `avatar`, `department`)
VALUES
	('dd45d7a0-b611-11e6-b8eb-5b7aab60e12d','chiqingzhen','迟庆祯',NULL,NULL),
	('dde91e20-ba93-11e6-a3bc-5d95ee3f44e7','zhaopenglai','赵朋来',NULL,NULL),
	('e84ed360-b614-11e6-846e-85ad7f6b98b5','gaoying08','高莹',NULL,NULL),
	('f1fbd050-b611-11e6-b8eb-5b7aab60e12d','lijian13','李健',NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
